<?php
    Class showPicClass{
        public function showPicDOOfHN($hn){
            $dir = "../../FRI/".$hn."/DO";

            // Open a directory, and read its contents
            if (is_dir($dir)){
                if ($dh = opendir($dir)){
                    while (($file = readdir($dh)) !== false){
                        if($file != "." AND $file != ".."){

                            //file type
			                $subFileType = explode(".", $file);
			                $fileTypeFromName = $subFileType[1];

                            if($fileTypeFromName == "jpg" OR $fileTypeFromName == "JPG" OR $fileTypeFromName == "jpeg" OR $fileTypeFromName == "JPEG" OR $fileTypeFromName == "png" OR $fileTypeFromName == "PNG" OR $fileTypeFromName == "gif" OR $fileTypeFromName == "GIF" OR $fileTypeFromName == "tif" OR $fileTypeFromName == "TIF"){
                                echo "filename:" . $file . "<br>";
                                echo "<img src='".$dir."/".$file."' class='img-responsive'>"."<br />";
                            }
                            
                        }
                    }
                    closedir($dh);
                }
            }
        }

        public function showPicONOfHN($hn){
            $dir = "../../FRI/".$hn."/ON";

            // Open a directory, and read its contents
            if (is_dir($dir)){
                if ($dh = opendir($dir)){
                    while (($file = readdir($dh)) !== false){
                        if($file != "." AND $file != ".."){

                            //file type
			                $subFileType = explode(".", $file);
			                $fileTypeFromName = $subFileType[1];

                            if($fileTypeFromName == "jpg" OR $fileTypeFromName == "JPG" OR $fileTypeFromName == "jpeg" OR $fileTypeFromName == "JPEG" OR $fileTypeFromName == "png" OR $fileTypeFromName == "PNG" OR $fileTypeFromName == "gif" OR $fileTypeFromName == "GIF" OR $fileTypeFromName == "tif" OR $fileTypeFromName == "TIF"){
                                echo "filename:" . $file . "<br>";
                                echo "<img src='".$dir."/".$file."' class='img-responsive'>"."<br />";
                            }
                            
                        }
                    }
                    closedir($dh);
                }
            }
        }
    } 

?>