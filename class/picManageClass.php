<?php
	Class picManageClass{

		public function uploadPic($tmpFile, $picName, $hn){
			//random File Name
			$randomBannerName = mt_rand(1, 99);
			
			//file type
			$subFileType = explode(".", $picName);
			$fileTypeFromName = $subFileType[1];
			
			$newPicName = date('Y-m-d-H-i-s').'num'.$hn.'ran'.$randomBannerName.'rw.'.$fileTypeFromName;
			
			//echo "../../FRI/".$hn."/img/caseDrawing/".$newPicName;
			$picPath = "../../FRI/".$hn."/img/caseDrawing/".$newPicName;
			if(move_uploaded_file($tmpFile, $picPath)){
				echo "<script>alert('"."อัพโหลดภาพสำเร็จ"."')</script>";
				return $newPicName;
			}else{
				echo "<script>alert('"."อัพโหลดภาพล้มเหลว โปรดติดต่อผู้ดูแลระบบ"."')</script>";
			}
		}

		public function uploadPicONDO($tmpFile, $picName, $hn, $typePic){
			//random File Name
			$randomBannerName = mt_rand(1, 99);
			
			//file type
			$subFileType = explode(".", $picName);
			$fileTypeFromName = $subFileType[1];
			
			$newPicName = date('Y-m-d-H-i-s').'num'.$hn.'ran'.$randomBannerName.'rw.'.$fileTypeFromName;
			
			//echo "../../FRI/".$hn."/".$typePic."/".$newPicName;

			if($typePic == "TEMP"){
				$picPath = "../../tempScan/".$newPicName;
			}else{
				$picPath = "../../FRI/".$hn."/".$typePic."/".$newPicName;
			}
			
			if(move_uploaded_file($tmpFile, $picPath)){
				echo "<script>alert('"."อัพโหลดภาพสำเร็จ"."')</script>";
				return $newPicName;
			}else{
				echo "<script>alert('"."อัพโหลดภาพล้มเหลว โปรดติดต่อผู้ดูแลระบบ"."')</script>";
			}
		}

		//job
		public function uploadPrdPic($uploadFileName, $fileNameRaw, $idRecord, $updateDatabase){
			$subFileType = explode(".", $uploadFileName);

			$fileType = $subFileType[1];

			$randomFileName = mt_rand(1, 99);
			$fileName = $idRecord.'rj'.$randomFileName;

			if($fileType == 'jpg' OR $fileType == 'JPG' OR $fileType == 'jpeg' OR $fileType == 'JPEG' OR $fileType == 'PNG' OR $fileType == 'png'){
				if(move_uploaded_file($fileNameRaw, "picJob/".$fileName.'.'.$fileType)){
					
					if($updateDatabase == TRUE){
						$path = $fileName.'.'.$fileType;
						$sql = "UPDATE job SET JOB_IMG_PATH = '$path' WHERE ID = '$idRecord'";
						mysql_query($sql);
					}
				}else{
					echo "<script language='javascript'>";
					echo "alert('เกิดข้อผิดพลาดในการอัพโหลดไฟล์ โปรดลองใหม่อีกครั้ง')";
					echo  "</script>";
					return "";
				}
			}else{
				echo "<script language='javascript'>";
				echo "alert('โปรดอัพโหลดเฉพาะรูปภาพนามสกุล .jpg หรือ .png เท่านั้น')";
				echo  "</script>";
			}
		}

		public function deletePicJob($ID){
			$sql = "SELECT JOB_IMG_PATH FROM job WHERE ID = '$ID'";
			$result = mysql_query($sql);
			while($fetcharr=mysql_fetch_array($result)){
            	$JOB_IMG_PATH = $fetcharr['JOB_IMG_PATH'];

            	$pathToDeleteFile = 'picJob/'.$JOB_IMG_PATH;
            	unlink("$pathToDeleteFile");
            }

			$sql2 = "UPDATE job SET JOB_IMG_PATH = null WHERE ID = '$ID';";
			$result2 = mysql_query($sql2);
		}

	}
?>