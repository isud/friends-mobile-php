<?php
    Class patientListClass{

		//แผนกที่รับคิวผู้ป่วย
		/*
		public function cmbExaminationRoom(){
			$sql = "SELECT clinic,clinicname FROM  masclinic where f_exm AND `status` = 1;";
			$result = mysql_query($sql);
			
			echo "<select class='form-control' name='cmbClinicExam'>";

			echo "<option value=''>".'--เลือกแผนก--'."</option>";
			while($rows = mysql_fetch_array($result)){
				echo "<option value='".$rows["clinic"]."'>".$rows["clinicname"]."</option>";
			}
			echo "</select>";
		}
		*/
		
		
        //รายการผู้ป่วย OPD
        public function patientOPDList(){
			//$sql = "SELECT c.`clinicid` AS 'CLINICID',CAST(h.hn AS CHAR(15)) AS HN,CASE WHEN (h.penid  = 0) THEN CONCAT(p.`name`,' ',p.lname) ELSE CONCAT(penname.`penname`,' ', penname.`penlname`)  END   AS `ชื่อ - นามสกุล`,cstatus AS '..',h.vn AS 'VN' , c.bclinic AS 'BCLINIC' FROM (SELECT penid,vn,hn FROM   frnservice WHERE f_discharge = 1 ) AS h INNER JOIN (SELECT * FROM  frnclinic WHERE clinic = (SELECT clinic FROM  hospemp WHERE empid = '$_SESSION[USER_ID]') AND cstatus IN (1,2,3,4) AND doctor ='$_SESSION[USER_ID]' ) AS c ON h.vn = c.vn JOIN  person AS p ON h.hn = p.hn LEFT JOIN (SELECT * FROM  maspenname WHERE `status` = 1) AS penname ON h.penid = penname.penid ORDER BY c.sentime ASC;";

			//$sql = "SELECT c.`clinicid` AS 'CLINICID',CAST(h.hn AS CHAR(15)) AS HN, CONCAT_WS('', CASE WHEN(p.`name` = '') THEN CONCAT_WS('',`seprename`,' ',p.`ename`) ELSE CONCAT_WS('',`ftprename`) END,'  ', CASE WHEN (h.penid = 0) THEN CONCAT_WS('',p.`name`,' ',p.lname) ELSE CONCAT_WS('',penname.`penname`,' ', penname.`penlname`) END) AS 'Name', cstatus AS 'cstatus',h.vn AS 'VN' , c.bclinic AS 'BCLINIC' , SEC_TO_TIME(  TIMESTAMPDIFF(SECOND,c.sentime,CURTIME() )) AS WaitTime FROM (SELECT penid,vn,hn FROM   frnservice WHERE f_discharge = 1 ) AS h INNER JOIN (SELECT * FROM  frnclinic WHERE bclinic = '$examinationRoom' AND clinic = 6 AND cstatus IN (1,2,3,4) AND doctor ='$_SESSION[USER_ID]' AND f_finish = 0) AS c ON h.vn = c.vn JOIN  person AS p ON h.hn = p.hn LEFT JOIN masprename ON p.prename = masprename.prename LEFT JOIN (SELECT * FROM  maspenname WHERE `status` = 1) AS penname ON h.penid = penname.penid ORDER BY c.sentime ASC;";

			$sql = "SELECT c.`clinicid` AS 'CLINICID',CAST(h.hn AS CHAR(15)) AS HN , CONCAT_WS('',CASE WHEN(p.`name` = '') THEN CONCAT_WS('',`seprename`,' ',p.`ename`) ELSE CONCAT_WS('',`ftprename`) END,'  ',CASE WHEN (h.penid = 0) THEN CONCAT_WS('',p.`name`,' ',p.lname) ELSE CONCAT_WS('',penname.`penname`,' ', penname.`penlname`) END) AS 'Name' ,cstatus AS 'cstatus',h.vn AS 'VN' , c.bclinic AS 'BCLINIC' , SEC_TO_TIME(  TIMESTAMPDIFF(SECOND,c.sentime,CURTIME() )) AS WaitTime , clinicname FROM (SELECT penid,vn,hn FROM   frnservice WHERE f_discharge = 1 ) AS h INNER JOIN (SELECT * FROM  frnclinic WHERE  clinic = 6 AND cstatus IN (1,2,3,4) AND doctor = '$_SESSION[USER_ID]' AND f_finish = 0) AS c ON h.vn = c.vn JOIN  person AS p ON h.hn = p.hn LEFT JOIN masprename ON p.prename = masprename.prename LEFT JOIN masclinic ON masclinic.clinic = c.bclinic LEFT JOIN (SELECT * FROM  maspenname WHERE `status` = 1) AS penname ON h.penid = penname.penid ORDER BY c.sentime ASC;";

			$result = mysql_query($sql);
			$numRows = mysql_num_rows($result);

			if($numRows == 0){
				echo "<h4>ยังไม่มีคิวผู้ป่วย</h4>";
			}else{
				echo "<table id='example1' class='table table-bordered table-striped table-hover'>";
					echo "<thead>";
			            echo "<tr>";
			              	echo "<th>HN</th>";
							echo "<th>VN</th>";
			              	echo "<th>ชื่อผู้ป่วย</th>";
							echo "<th>Action</th>";
			            echo "</tr>";
			        echo "</thead>";

				while($rows = mysql_fetch_array($result)){
					$HN = $rows["HN"];
					$VN = $rows["VN"];
					$patName = $rows["ชื่อ - นามสกุล"];

					echo "<tr>";
		              	echo "<td>$HN</td>";
						echo "<td>$VN</td>";
		              	echo "<td>$patName</td>";

		              	echo "<td><a class='btn btn-default' href='opdPatDetail.php?HN=$HN&VN=$VN'>ดูข้อมูล</a></td>";
		            echo "</tr>";
				}
				echo "</table>";
			}
		}

		//รายการผู้ป่วย IPD
        public function patientIPDList(){
			$sql = "SELECT CAST(fa.an AS CHAR(12)) AS 'AN', fa.hn AS 'HN', CONCAT(IFNULL(mpn.ftprename, ''), ' ', IFNULL(person.`name`, ''), ' ', IFNULL(person.`lname`, '')) AS 'PATNAME',frnshift.`bedsid`, sroomitem.`bedsname`, CONCAT(ROUND(DATEDIFF(NOW(), person.birth)/365)) AS 'age', fa.datetime_admit,hospemp.`empid`,hospemp.`DOCNAME`, frnservice.`chiefcomp`, diagopd.`drnote` , fa.f_lock AS 'LOCK' FROM (SELECT an, hn, vn, warddisch, f_lock, datetime_admit,dr FROM frnadmission WHERE f_discharge = '1') AS fa JOIN (SELECT hn, `name`, `lname`, `sex`, `prename`, birth FROM person) AS person ON person.hn = fa.hn LEFT JOIN masprename AS mpn ON mpn.prename = person.prename LEFT JOIN (SELECT an, bedsid FROM frnshift WHERE intime IS NOT NULL AND outtime IS NULL) AS frnshift ON frnshift.an = fa.an LEFT JOIN (SELECT bedsid, bedsname FROM sroomitem ) AS sroomitem ON sroomitem.`bedsid` = frnshift.bedsid LEFT JOIN (SELECT CONCAT_WS(' ',`name`,lname) AS DOCNAME,`empid` FROM hospemp) AS hospemp ON hospemp.`empid` = fa.`dr` LEFT JOIN (SELECT hn, vn, drnote FROM diagopd WHERE (TRIM(drnote) <> '' AND drnote IS NOT NULL) AND hn IS NOT NULL) AS diagopd ON diagopd.hn = person.`hn` LEFT JOIN (SELECT `chiefcomp`,`vn` FROM frnservice ) AS frnservice ON frnservice.`vn` = fa.`vn` GROUP BY fa.hn ORDER BY sroomitem.`bedsname` ASC ";

			$result = mysql_query($sql);
			$numRows = mysql_num_rows($result);

			if($numRows == 0){
				echo "<h4>ยังไม่มีผู้ป่วยใน</h4>";
			}else{
				echo "<table id='example1' class='table table-bordered table-striped table-hover'>";
					echo "<thead>";
			            echo "<tr>";
			              	echo "<th>HN</th>";
							echo "<th>AN</th>";
			              	echo "<th>ชื่อผู้ป่วย</th>";
							echo "<th>Action</th>";
			            echo "</tr>";
			        echo "</thead>";

				while($rows = mysql_fetch_array($result)){
					$HN = $rows["HN"];
					$AN = $rows["AN"];
					$patName = $rows["PATNAME"];

					echo "<tr>";
		              	echo "<td>$HN</td>";
						echo "<td>$AN</td>";
		              	echo "<td>$patName</td>";

		              	echo "<td><a class='btn btn-default' href='ipdPatDetail.php?HN=$HN&AN=$AN'>ดูข้อมูล</a></td>";
		            echo "</tr>";
				}
				echo "</table>";
			}
		}
    }
?>