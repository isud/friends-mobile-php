<?php
	include "resumeClass.php";

	Class userClass{
		public $USER_ID;
		public $USER_ACCOUNT;
		public $USER_PASSWORD;
		public $USER_NAME;
		public $USER_LNAME;
		public $USER_TEL;
		public $USER_EMAIL;
		public $DATE_ADD;
		public $DATE_EDIT;
		public $F_ADMIN;
		public $STATUS;

		public $PRENAME_ID;
		public $USER_NICKNAME;
	
		
		//ตารางหน้า admin
		public function userList(){
			$sql = "SELECT `USER_ID`, `USER_ACCOUNT`, CONCAT_WS(' ', USER_NAME, USER_LNAME) as 'NAME', `USER_TEL`, `USER_EMAIL`, F_ADMIN, `STATUS` FROM user";
			$result=mysql_query($sql);
			$numRow = mysql_num_rows($result);
			
			if($numRow == 0){
				echo "<script language='javascript'>";
				echo "alert('ยังไม่มีผู้ใช้งานอยู่ในระบบ')";
				echo  "</script>";
				//echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
			}else{
				echo "<table id='example1' class='table table-bordered table-striped table-hover'>";
					echo "<thead>";
						echo "<tr>";
							echo "<th>รหัสผู้ใช้</th>";
							echo "<th>USERNAME</th>";
							echo "<th>ชื่อ-สกุล</th>";
							echo "<th>โทรศัพท์</th>";
							echo "<th>อีเมล์</th>";
							echo "<th>สถานะ</th>";
							echo "<th>ประเภท</th>";
							echo "<th>action</th>";
						echo "</tr>";
					echo "</thead>";
					echo "<tbody>";
				
					while($fetcharr=mysql_fetch_array($result)){
						$USER_ID = $fetcharr['USER_ID'];
						$USER_ACCOUNT = $fetcharr['USER_ACCOUNT'];
						$NAME = $fetcharr['NAME'];
						$USER_TEL = $fetcharr['USER_TEL'];
						$USER_EMAIL = $fetcharr['USER_EMAIL'];
						$F_ADMIN = $fetcharr['F_ADMIN'];
						$STATUS = $fetcharr['STATUS'];
						
						echo "<tr>";
							echo "<td>".$USER_ID."</td>";
							echo "<td>$USER_ACCOUNT</td>";
							echo "<td>$NAME</td>";
							echo "<td>$USER_TEL</td>";
							echo "<td>$USER_EMAIL</td>";

							if($F_ADMIN == 1){
								echo "<td><center>Admin</center></td>";
							}else{
								echo "<td><center>User</center></td>";
							}

							if($STATUS == 1){
								echo "<td class='success'><center>เปิดใช้งาน</center></td>";
							}else{
								echo "<td class='danger'><center>ระงับใช้งาน</center></td>";
							}

							echo "<td><a href='userEdit.php?USER_ID=$USER_ID' class='btn btn-warning'><i class='fa fa-eye'> แก้ไขข้อมูล</a></td>";
						echo "</tr>";
					}
					echo "</tbody>";
					/*
					echo "<tfoot>";
						echo "<tr>";
							echo "<th>รหัสผู้ใช้</th>";
							echo "<th>USERNAME</th>";
							echo "<th>ชื่อผู้ใช้</th>";
							echo "<th>โทรศัพท์</th>";
							echo "<th>อีเมล์</th>";
							echo "<th>สถานะ</th>";
							echo "<th>ประเภท</th>";
							echo "<th>action</th>";
						echo "</tr>";
					echo "</tfoot>";
					*/
				echo "</table>";
			}
		}
		
		//ตรวจ username ว่าเป็นตัวเลขและอักษรหรือไม่
		public function checkUserRegex($username){
			if (ctype_alnum($username)) {
				return true;
			} else {
				return false;
			}
		}

		//ตรวจว่า username มีอยู่แล้วหรือยัง
		//ใช้สำหรับ add
		public function checkExistingUsername($USER_ACCOUNT){
			$sql = "SELECT USER_ID FROM user WHERE USER_ACCOUNT = '$USER_ACCOUNT';";
			$result = mysql_query($sql);
			$numRows = mysql_num_rows($result);

			if($numRows == 0){
				return true;
			}else{
				return false;
			}
		}

		//ตรวจว่า username มีอยู่แล้วหรือยัง
		//ใช้สำหรับ edit
		public function checkExistingUsernameEdit($USER_ID, $USER_ACCOUNT){
			$sql = "SELECT USER_ID FROM user WHERE USER_ACCOUNT = '$USER_ACCOUNT' AND USER_ID <> '$USER_ID';";
			$result = mysql_query($sql);
			$numRows = mysql_num_rows($result);

			if($numRows == 0){
				return true;
			}else{
				return false;
			}
		}

		//เพิ่ม user หน้า admin
		public function addUser($USER_ACCOUNT, $USER_PASSWORD, $CONFIRM_PASSWORD, $USER_NAME, $USER_LNAME, $USER_TEL, $USER_EMAIL, $STATUS){

			if($USER_ACCOUNT == ""){
				echo "<script language='javascript'>";
				echo "alert('โปรดระบุ Username')";
				echo  "</script>";
				return false;
			}else{
				if(strlen($USER_ACCOUNT) < 8){
					echo "<script language='javascript'>";
					echo "alert('ชื่อผู้ใช้ต้องมีความยาว อย่างน้อย 8 ตัวอักษร')";
					echo  "</script>";
					return false;
				}else{
					if(strlen($USER_PASSWORD) < 8){
						echo "<script language='javascript'>";
						echo "alert('รหัสผ่านต้องมีความยาว อย่างน้อย 8 ตัวอักษร')";
						echo  "</script>";
						return false;
					}else{
						if($this->checkUserRegex($USER_ACCOUNT) == false){
							echo "<script language='javascript'>";
							echo "alert('ชื่อผู้ใช้ต้องประกอบด้วย อักขระและตัวอักษรเท่านั้น')";
							echo  "</script>";
							return false;
						}else{
							if($this->checkUserRegex($USER_PASSWORD) == false){
								echo "<script language='javascript'>";
								echo "alert('รหัสผ่านต้องประกอบด้วย อักขระและตัวอักษรเท่านั้น')";
								echo  "</script>";
								return false;
							}else{
								if($this->checkExistingUsername($USER_ACCOUNT) == false){
									echo "<script language='javascript'>";
									echo "alert('มีชื่อผู้ใช้ดังกล่าวอยู่ในระบบอยู่แล้ว')";
									echo  "</script>";
									return false;
								}else{
									if($USER_PASSWORD == ""){
										echo "<script language='javascript'>";
										echo "alert('โปรดระบุ Password')";
										echo  "</script>";
										return false;
									}else{
										if($USER_PASSWORD !== $CONFIRM_PASSWORD){
											echo "<script language='javascript'>";
											echo "alert('โปรดระบุ Password และ Confirm Password ให้ตรงกัน')";
											echo  "</script>";
											return false;
										}else{
											if($USER_NAME == ""){
												echo "<script language='javascript'>";
												echo "alert('โปรดระบุชื่อ')";
												echo  "</script>";
												return false;
											}else{
												if($USER_LNAME == ""){
													echo "<script language='javascript'>";
													echo "alert('โปรดระบุนามสกุล')";
													echo  "</script>";
													return false;
												}else{
													try {
														$sql1 = "";
														$sql2 = "";

														$sql1 .= "USER_ACCOUNT, ";
														$sql2 .= "'$USER_ACCOUNT', ";

														$sql1 .= "USER_PASSWORD, ";
														$sql2 .= "'$USER_PASSWORD', ";

														$sql1 .= "USER_NAME, ";
														$sql2 .= "'$USER_NAME', ";

														$sql1 .= "USER_LNAME, ";
														$sql2 .= "'$USER_LNAME', ";

														$sql1 .= "USER_TEL, ";
														if($USER_TEL !== ""){
															$sql2 .= "'$USER_TEL', ";
														}else{
															$sql2 .= "null, ";
														}

														$sql1 .= "USER_EMAIL, ";
														if($USER_EMAIL !== ""){
															$sql2 .= "'$USER_EMAIL', ";
														}else{
															$sql2 .= "null, ";
														}

														$sql1 .= "DATE_ADD, ";
														$sql2 .= "NOW(), ";

														$sql1 .= "STATUS";
														$sql2 .= "'$STATUS'";

														$sql = "INSERT INTO user (".$sql1.") VALUES (".$sql2.");";

														$result = mysql_query($sql);

														$USER_ID = mysql_insert_id();

														$objAddNewRowResumeTemplate = new resumeClass();
														$objAddNewRowResumeTemplate->addUserResume($USER_ID, $USER_NAME, $USER_LNAME, $USER_TEL, $USER_EMAIL);

														return true;
													} catch (Exception $e) {
														echo 'Caught exception: ',  $e->getMessage(), "\n";
														echo "<script language='javascript'>";
														echo "alert('.$e->getMessage().')";
														echo "</script>";
														return false;
													}
													
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		//แก้ user หน้า admin
		public function editUser($USER_ID, $USER_ACCOUNT, $USER_PASSWORD, $CONFIRM_PASSWORD, $USER_NAME, $USER_LNAME, $USER_TEL, $USER_EMAIL, $STATUS){
			if($USER_ACCOUNT == ""){
				echo "<script language='javascript'>";
				echo "alert('โปรดระบุ Username')";
				echo  "</script>";
				return false;
			}else{
				if(strlen($USER_ACCOUNT) < 8){
					echo "<script language='javascript'>";
					echo "alert('ชื่อผู้ใช้ต้องมีความยาว อย่างน้อย 8 ตัวอักษร')";
					echo  "</script>";
					return false;
				}else{
					if(strlen($USER_PASSWORD) < 8){
						echo "<script language='javascript'>";
						echo "alert('รหัสผ่านต้องมีความยาว อย่างน้อย 8 ตัวอักษร')";
						echo  "</script>";
						return false;
					}else{
						if($this->checkUserRegex($USER_ACCOUNT) == false){
							echo "<script language='javascript'>";
							echo "alert('ชื่อผู้ใช้ต้องประกอบด้วย อักขระและตัวอักษรเท่านั้น')";
							echo  "</script>";
							return false;
						}else{
							if($this->checkUserRegex($USER_PASSWORD) == false){
								echo "<script language='javascript'>";
								echo "alert('รหัสผ่านต้องประกอบด้วย อักขระและตัวอักษรเท่านั้น')";
								echo  "</script>";
								return false;
							}else{
								if($this->checkExistingUsernameEdit($USER_ID, $USER_ACCOUNT) == false){
									echo "<script language='javascript'>";
									echo "alert('มีชื่อผู้ใช้ดังกล่าวอยู่ในระบบอยู่แล้ว')";
									echo  "</script>";
									return false;
								}else{
									if($USER_PASSWORD == ""){
										echo "<script language='javascript'>";
										echo "alert('โปรดระบุ Password')";
										echo  "</script>";
										return false;
									}else{
										if($USER_PASSWORD !== $CONFIRM_PASSWORD){
											echo "<script language='javascript'>";
											echo "alert('โปรดระบุ Password และ Confirm Password ให้ตรงกัน')";
											echo  "</script>";
											return false;
										}else{
											if($USER_NAME == ""){
												echo "<script language='javascript'>";
												echo "alert('โปรดระบุชื่อ')";
												echo  "</script>";
												return false;
											}else{
												if($USER_LNAME == ""){
													echo "<script language='javascript'>";
													echo "alert('โปรดระบุนามสกุล')";
													echo  "</script>";
													return false;
												}else{
													if($USER_ID == ""){
														echo "<script language='javascript'>";
														echo "alert('ผิดพลาด ไม่มีการแก้ไขข้อมูล')";
														echo  "</script>";
														return false;
													}else{
														$sql = "UPDATE user SET ";

														$sql .= "USER_PASSWORD = '$USER_PASSWORD', ";
														$sql .= "USER_NAME = '$USER_NAME', ";
														$sql .= "USER_LNAME = '$USER_LNAME', ";
														$sql .= "USER_TEL = '$USER_TEL', ";
														$sql .= "USER_EMAIL = '$USER_EMAIL', ";
														$sql .= "STATUS = '$STATUS', ";
														$sql .= "DATE_EDIT = NOW() ";

														$sql .= "WHERE USER_ID = '$USER_ID';";

														$result = mysql_query($sql);

														return true;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		//ดึงข้อมูล user รายคน
		public function getUserInfo($USER_ID){
			$sql = "SELECT * FROM user WHERE USER_ID = '$USER_ID'";
			$result = mysql_query($sql);
			$numRow = mysql_num_rows($result);
			
			if($numRow == 0){
				echo "<script language='javascript'>";
				echo "alert('ไม่พบรายละเอียดข้อมูลของผู้ใช้รายนี้')";
				echo  "</script>";
				echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
			}else{
				while($fetcharr=mysql_fetch_array($result)){
					$USER_ID = $fetcharr['USER_ID'];
					$USER_ACCOUNT = $fetcharr['USER_ACCOUNT'];
					$USER_PASSWORD = $fetcharr['USER_PASSWORD'];
					$USER_NAME = $fetcharr['USER_NAME'];
					$USER_LNAME = $fetcharr['USER_LNAME'];
					$USER_TEL = $fetcharr['USER_TEL'];
					$USER_EMAIL = $fetcharr['USER_EMAIL'];
					$DATE_ADD = $fetcharr['DATE_ADD'];
					$DATE_EDIT = $fetcharr['DATE_EDIT'];
					$F_ADMIN = $fetcharr['F_ADMIN'];
					$STATUS = $fetcharr['STATUS'];
				}

				$this->USER_ID = $USER_ID;
				$this->USER_ACCOUNT = $USER_ACCOUNT;
				$this->USER_PASSWORD = $USER_PASSWORD;
				$this->USER_NAME = $USER_NAME;
				$this->USER_LNAME = $USER_LNAME;
				$this->USER_TEL = $USER_TEL;
				$this->USER_EMAIL = $USER_EMAIL;
				$this->DATE_ADD = $DATE_ADD;
				$this->DATE_EDIT = $DATE_EDIT;
				$this->F_ADMIN = $F_ADMIN;
				$this->STATUS = $STATUS;
			}
		}

		public function adminLogin($USER_ACCOUNT, $USER_PASSWORD){
			$sql = "SELECT * FROM admin WHERE `USER_ACCOUNT` = '$USER_ACCOUNT' AND `USER_PASSWORD` = '$USER_PASSWORD';";
			$result=mysql_query($sql);
			$numRow = mysql_num_rows($result);
			
			if($numRow == 0){
				echo "<script language='javascript'>";
				echo "alert('ท่านกรอก username หรือ password ไม่ถูกต้อง โปรดกรุณาระบุใหม่อีกครั้ง')";
				echo  "</script>";
				echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
			}else{
				while($fetcharr=mysql_fetch_array($result)){
					$ADMIN_USER_ID = $fetcharr['USER_ID'];
					$ADMIN_USER_ACCOUNT = $fetcharr['USER_ACCOUNT'];
					$ADMIN_USER_PASSWORD = $fetcharr['USER_PASSWORD'];
					$ADMIN_USER_NAME = $fetcharr['USER_NAME'].' '.$fetcharr['USER_LNAME'];
					$ADMIN_STATUS = $fetcharr['STATUS'];
				}
				if($ADMIN_STATUS == 1){
					$_SESSION['ADMIN_USER_ID'] = $ADMIN_USER_ID;
					$_SESSION['ADMIN_USER_ACCOUNT'] = $ADMIN_USER_ACCOUNT;
					$_SESSION['ADMIN_USER_NAME'] = $ADMIN_USER_NAME;
					$_SESSION['ADMIN_STATUS'] = $ADMIN_STATUS;
					echo "<script language=\"javascript\">window.location='".basename("dashboard.php")."'</script>";
				}else{
					echo "<script language='javascript'>";
					echo "alert('บัญชีใช้งานของท่านถูกระงับการใช้งาน โปรดติดต่อผู้ดูแลระบบ')";
					echo  "</script>";
					echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
				}
				
			}
		}
	}
?>