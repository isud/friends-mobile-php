<?php
	echo "<header class='main-header'>";
		echo "<a class='logo'><b>FRIENDS</b> Mobile</a>";
		//<!-- Header Navbar: style can be found in header.less -->
		echo "<nav class='navbar navbar-static-top' role='navigation'>";
			//<!-- Sidebar toggle button-->
			echo "<a href='#' class='sidebar-toggle' data-toggle='offcanvas' role='button'>";
				echo "<span class='sr-only'>Toggle navigation</span>";
				echo "<span class='icon-bar'></span>";
				echo "<span class='icon-bar'></span>";
				echo "<span class='icon-bar'></span>";
			echo "</a>";
			echo "<div class='navbar-custom-menu'>";
				echo "<ul class='nav navbar-nav'>";
					//<!-- Messages: style can be found in dropdown.less-->
					
					//<!-- Notifications: style can be found in dropdown.less -->
					echo "<li class='dropdown notifications-menu'>";
						//echo "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>";
							//echo "<i class='fa fa-bell-o'></i>";
							//echo "<span class='label label-warning'>10</span>";
						//echo "</a>";
						echo "<ul class='dropdown-menu'>";
							//echo "<li class='header'>You have 10 notifications</li>";
							echo "<li>";
								//<!-- inner menu: contains the actual data -->
								echo "<ul class='menu'>";
									echo "<li>";
										echo "<a href='#'>";
											echo "<i class='fa fa-users text-aqua'></i> 5 new members joined today";
										echo "</a>";
									echo "</li>";
								echo "</ul>";
							echo "</li>";
							echo "<li class='footer'><a href='#'>View all</a></li>";
						echo "</ul>";
					echo "</li>";
					//<!-- Tasks: style can be found in dropdown.less -->
					
					//<!-- User Account: style can be found in dropdown.less -->
					echo "<li class='dropdown user user-menu'>";
						echo "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>";
							echo "<span class='hidden-xs'>".$_SESSION['USER_NAME']."</span>";
						echo "</a>";
						echo "<ul class='dropdown-menu'>";
							//<!-- User image -->
							echo "<li class='user-header'>";
								echo "<p>";
									echo $_SESSION['USER_NAME'];
									//echo "<small>Member since Nov. 2012</small>";
								echo "</p>";
							echo "</li>";
							//<!-- Menu Body -->
							
							//<!-- Menu Footer-->
							echo "<li class='user-footer'>";
								echo "<div class='pull-right'>";
									echo "<a href='logOut.php' class='btn btn-default btn-flat'>Sign out</a>";
								echo "</div>";
							echo "</li>";
						echo "</ul>";
					echo "</li>";
				echo "</ul>";
			echo "</div>";
		echo "</nav>";
	echo "</header>";
?>