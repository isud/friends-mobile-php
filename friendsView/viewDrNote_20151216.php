<!DOCTYPE html>
<html lang="en">
	<head>
		<?php
			include "include/connectdb.php";
			
			function convertDateBD($date){
				//$dateTimeExplode = explode(" ", $dateTime);
				//$date = $dateTimeExplode[0];
				//$time = $dateTimeExplode[1];

				$dateExplode = explode('-', $date);
				//$timeExplode = explode(':', $time);

				//convert year
				$bdYear = $dateExplode[0] + 543;
				//convert month
				switch ($dateExplode[1]) {
					case 01:
						$bdMonth = 'มกราคม';
						break;
					case 02:
						$bdMonth = 'กุมภาพันธ์';
						break;
					case 03:
						$bdMonth = 'มีนาคม';
						break;
					case 04:
						$bdMonth = 'เมษายน';
						break;
					case 05:
						$bdMonth = 'พฤษภาคม';
						break;
					case 06:
						$bdMonth = 'มิถุนายน';
						break;
					case 07:
						$bdMonth = 'กรกฎาคม';
						break;
					case 08:
						$bdMonth = 'สิงหาคม';
						break;
					case 09:
						$bdMonth = 'กันยายน';
						break;
					case 10:
						$bdMonth = 'ตุลาคม';
						break;
					case 11:
						$bdMonth = 'พฤศจิกายน';
						break;
					case 12:
						$bdMonth = 'ธันวาคม';
						break;
				}
				$bdDate = $dateExplode[2];
				//$time = $time;
				
				return $bdDate." ".$bdMonth." ".$bdYear;
			}
		?>
		<meta charset="utf-8">
		<!-- Clear Cache -->
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
		<!--End of Clear Cache -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>FRIENDS's View Dr.Note</title>

		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<style>
			#drnote{
				color: navy;
			}
			
			#nursenote{
				color: #ce2f00;
			}
			
			#drug{
				color: olive;
			}
			
			#sign{
				color: black;
			}
			
			hr{
				border: 0;
				height: 1px;
				background: #333;
				background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
				background-image:    -moz-linear-gradient(left, #ccc, #333, #ccc);
				background-image:     -ms-linear-gradient(left, #ccc, #333, #ccc);
				background-image:      -o-linear-gradient(left, #ccc, #333, #ccc);
			}
			
			blockquote{
				font-size: 14px;
			}
			
			span#appointment{
				color: #CF0000;
			}
			
			pre{
				font-size: 14px;
			}
			
			pre{
				color: navy;
				font-size: 100%;
				font: inherit;
				padding: 0px;
				background-color: #fff;
				border: none;
				margin: 0px 0px 0px 0px;
			}
		</style>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="js/html5shiv.min.js"></script>
		  <script src="js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-sm-12'>
					<?php
						//vitalsign
						$sqlVitalSign = "SELECT opd.VSID,RESULTVS,VSDESC,LL,HL,MC,F_FT,unit,p.NATION,mna.NATIONDESC,fl.FEELING,pc.TELEPHONE AS TELEPHONEPC,pc.MOBILE AS MOBILEPC,dru.DRUGALLERGY AS 'แพ้ยา',dst.DISEASE AS 'โรคประจำตัว' FROM (SELECT * FROM frnvsopd WHERE(VN = '$_GET[VN]') ) AS opd LEFT JOIN  (SELECT * FROM masvstype WHERE(`STATUS` = 1)) AS vst ON opd.VSID = vst.VSID LEFT JOIN frnservice AS se ON opd.VN = se.VN LEFT JOIN masfeeling AS fl ON se.FEELID = fl.FEELID LEFT JOIN (SELECT * FROM person WHERE(HN = '$_GET[HN]' AND `STATUS` = 1))  AS p ON se.HN = p.HN LEFT JOIN (SELECT * FROM masnation WHERE(`STATUS` = 1)) AS mna ON p.NATION = mna.NATION LEFT JOIN (SELECT * FROM personcontact) AS pc ON p.HN = pc.HN LEFT JOIN (SELECT HN, GROUP_CONCAT(DRUGALLERGY SEPARATOR ',') AS DRUGALLERGY FROM drugallergy GROUP BY HN) AS dru ON p.HN = dru.HN LEFT JOIN (SELECT HN, GROUP_CONCAT(DISEASE SEPARATOR ',') AS DISEASE FROM condisease WHERE(`STATUS` = 1) GROUP BY HN) AS dst ON p.HN = dst.HN ORDER BY lstorder";
						$resultVitalsign = mysql_query($sqlVitalSign);
						$vsNumRows = mysql_num_rows($resultVitalsign);
						
						echo "<br>";
						//echo "<strong>";
						if($vsNumRows > 0){
							
							$flagNon = "";
							$flagCol = 1;
							
							while($rowVS = mysql_fetch_array($resultVitalsign) ){
								$VSDESC = $rowVS["vsdesc"];
								$RESULTVS = $rowVS["resultvs"];
								$UNIT = $rowVS["unit"];
								$MC = $rowVS["mc"];
								$F_FT = $rowVS["f_ft"];
								
								if($MC == ""){
									if($F_FT == 1){
										if(($flagCol % 4) == 0.25){
											echo $flagCol % 4;
											echo 'test';
										}
										
										echo "<strong>".$VSDESC." : ".number_format($RESULTVS, 2)."</strong>"." "."<small>".$UNIT."</small>"."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";
									}else{
										echo "<strong>".$VSDESC." : ".number_format($RESULTVS, 0)."</strong>"." "."<small>".$UNIT."</small>"."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";
									}
								}else{
									if($MC != $flagNon){
										$sqlDoubleDataVS = "SELECT opd.VSID,RESULTVS,VSDESC,MC,unit,F_FT FROM (SELECT * FROM frnvsopd WHERE(VN = '$_GET[VN]') ) AS opd LEFT JOIN  (SELECT * FROM masvstype WHERE(`STATUS` = 1)) AS vst on opd.VSID = vst.VSID LEFT JOIN frnservice AS se ON opd.VN = se.VN LEFT JOIN masfeeling AS fl ON se.FEELID = fl.FEELID LEFT JOIN (SELECT * FROM person WHERE(HN = '$_GET[HN]' AND `STATUS` = 1))  AS p ON se.HN = p.HN LEFT JOIN (SELECT * FROM masnation WHERE(`STATUS` = 1)) AS mna ON p.NATION = mna.NATION LEFT JOIN (SELECT * FROM personcontact) AS pc ON p.HN = pc.HN LEFT JOIN (SELECT HN, GROUP_CONCAT(DRUGALLERGY SEPARATOR ',') AS DRUGALLERGY FROM drugallergy GROUP BY HN) AS dru ON p.HN = dru.HN LEFT JOIN (SELECT HN, GROUP_CONCAT(DISEASE SEPARATOR ',') AS DISEASE FROM condisease WHERE(`STATUS` = 1) GROUP BY HN) AS dst ON p.HN = dst.HN WHERE mc = '$MC' ORDER BY lstorder";
									
										$resultDoubleDataVS = mysql_query($sqlDoubleDataVS);
										
										$DoubleDataVSNumRows = mysql_num_rows($resultDoubleDataVS);
										$i = 1;
										while($rowDoubleDataVS = mysql_fetch_array($resultDoubleDataVS) ){
											$VSDESC = $rowDoubleDataVS["vsdesc"];
											$RESULTVS = $rowDoubleDataVS["resultvs"];
											$F_FT = $rowDoubleDataVS["f_ft"];
											$UNIT = $rowDoubleDataVS["unit"];
											
											if($DoubleDataVSNumRows != $i){
												if($F_FT == 1){
													echo "<strong>".$VSDESC." ".number_format($RESULTVS, 2)." / "."</strong>";
												}else{
													echo "<strong>".$VSDESC." ".number_format($RESULTVS, 0)." / "."</strong>";
												}
											}else{
												if($F_FT == 1){
													echo "<strong>".number_format($RESULTVS, 2)."</strong>"." "."<small>".$UNIT."</small>"."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";
												}else{
													echo "<strong>".number_format($RESULTVS, 0)."</strong>"." "."<small>".$UNIT."</small>"."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";
												}
											}
											$i++;
										}
										$flagNon = $MC;
									}
								}
								$flagCol++;
							}
							
						}
						//echo "</strong>";
						
						echo "<br><br>";
						
						$sqlCC = "SELECT chiefcomp FROM frnservice WHERE hn = '$_GET[HN]' AND vn = '$_GET[VN]' AND f_discharge = '1';";
						$resultCC = mysql_query($sqlCC);
						$numRowCC = mysql_num_rows($resultCC);
						
						if($numRowCC > 0){
							while($rowCC = mysql_fetch_array($resultCC) ){
								$chiefcomp = $rowCC["chiefcomp"];
								
								echo 'CC: '.$chiefcomp;
							}
						}
						
						echo "<br><br>";
						
						//drnote
						echo "<span id='drnote'>";
						$sqlDrnote = "SELECT a.`drid`, a.`VN` AS 'VN', a.`DRNOTE` AS 'DRNOTE', a.`PROVIDER` AS 'PROVIDER', CONCAT_WS(' ', b.`NAME`, b.`LNAME`) AS 'PROVIDERNAME', a.`D_UPDATE` AS 'D_UPDATE' FROM diagopd a LEFT JOIN hospemp b ON a.PROVIDER = b.EMPID WHERE a.`VN` = '$_GET[VN]' AND a.`STATUS` = '1' ORDER BY a.`D_UPDATE` DESC";
						$resultsqlDrnote = mysql_query($sqlDrnote);
						$drnoteNumRows = mysql_num_rows($resultsqlDrnote);
						
						if($drnoteNumRows > 0){
							echo "<p><strong>[Doctor]</strong></p>";
							while($row = mysql_fetch_array($resultsqlDrnote) ){
								$DRID = $row["drid"];
								$VN = $row["VN"];
								$DRNOTE = $row["DRNOTE"];
								$PROVIDER = $row["PROVIDER"];
								$PROVIDERNAME = $row["PROVIDERNAME"];
								$D_UPDATE = $row["D_UPDATE"];
								
								echo "<blockquote>";
								echo "<pre>".$DRNOTE."</pre>";
								echo "<br>";
								
								//checkup
								$sqlGetCheckup = "SELECT (SELECT pvalue FROM masdiagop WHERE art='GA_1' AND VALUE=diagopd.ga_1 ) AS GA_1,(SELECT pvalue FROM masdiagop WHERE art='GA_2' AND VALUE=diagopd.ga_2) AS GA_2,(SELECT pvalue FROM masdiagop WHERE art='GA_3' AND VALUE=diagopd.ga_3) AS GA_3,(SELECT pvalue FROM masdiagop WHERE art='GA_4' AND VALUE=diagopd.ga_4) AS GA_4,(SELECT pvalue FROM masdiagop WHERE art='GA_5' AND VALUE=diagopd.ga_5) AS GA_5,(SELECT pvalue FROM masdiagop WHERE art='GA_6' AND VALUE=diagopd.ga_6) AS GA_6,(SELECT pvalue FROM masdiagop WHERE art='GA_7' AND VALUE=diagopd.ga_7) AS GA_7,(SELECT pvalue FROM masdiagop WHERE art='hydro' AND VALUE=diagopd.hydro) AS hydro,gcs,gcs_e,gcs_m,gcs_v,(SELECT pvalue FROM masdiagop WHERE art='ln' AND VALUE=diagopd.ln) AS LN,(SELECT pvalue FROM masdiagop WHERE art='eye_1' AND VALUE=diagopd.eye_1) AS eye_1,ga_other,(SELECT pvalue FROM masdiagop WHERE art='heent_1' AND VALUE=diagopd.heent_1) AS heent_1,(SELECT pvalue FROM masdiagop WHERE art='heent_2' AND VALUE=diagopd.heent_2) AS heent_2,(SELECT pvalue FROM masdiagop WHERE art='heent_3' AND VALUE=diagopd.heent_3) AS heent_3,(SELECT pvalue FROM masdiagop WHERE art='heent_4' AND VALUE=diagopd.heent_4) AS heent_4,(SELECT pvalue FROM masdiagop WHERE art='eye_2' AND VALUE=diagopd.eye_2) AS eye_2,(SELECT pvalue FROM masdiagop WHERE art='eye_3' AND VALUE=diagopd.eye_3) AS eye_3,(SELECT pvalue FROM masdiagop WHERE art='eye_4' AND VALUE=diagopd.eye_4) AS eye_4,(SELECT pvalue FROM masdiagop WHERE art='tonsil' AND VALUE=diagopd.tonsil) AS tonsil,ts_other,(SELECT pvalue FROM masdiagop WHERE art='thyroid' AND VALUE=diagopd.thyroid) AS thyroid,tr_other,(SELECT pvalue FROM masdiagop WHERE art='beat' AND VALUE=diagopd.beat) AS beat,(SELECT pvalue FROM masdiagop WHERE art='mur' AND VALUE=diagopd.mur) AS mur,s_grade,s_area,d_grade,d_area,(SELECT pvalue FROM masdiagop WHERE art='lung' AND VALUE=diagopd.lung) AS lung,(SELECT pvalue FROM masdiagop WHERE art='l_wheez' AND VALUE=diagopd.l_wheez) AS l_wheez,(SELECT pvalue FROM masdiagop WHERE art='l_rhon' AND VALUE=diagopd.l_rhon) AS l_rhon,(SELECT pvalue FROM masdiagop WHERE art='crep_fine' AND VALUE=diagopd.crep_fine) AS crep_fine,(SELECT pvalue FROM masdiagop WHERE art='crep_coarse' AND VALUE=diagopd.crep_coarse) AS crep_coarse,l_other,(SELECT pvalue FROM masdiagop WHERE art='contour' AND VALUE=diagopd.contour) AS contour,(SELECT pvalue FROM masdiagop WHERE art='bowel' AND VALUE=diagopd.bowel) AS bowel,(SELECT pvalue FROM masdiagop WHERE art='knock' AND VALUE=diagopd.knock) AS knock,(SELECT pvalue FROM masdiagop WHERE art='palp' AND VALUE=diagopd.palp) AS palp,AREA,pal_other,(SELECT pvalue FROM masdiagop WHERE art='skin' AND VALUE=diagopd.skin) AS skin,skin_other,(SELECT pvalue FROM masdiagop WHERE art='extrem' AND VALUE=diagopd.extrem) AS extrem,ten_at,extre_other,(SELECT pvalue FROM masdiagop WHERE art='neuro' AND VALUE=diagopd.neuro) AS neuro,mg_up_l,mg_up_r,mg_lo_l,mg_lo_r,cn,(SELECT pvalue FROM masdiagop WHERE art='cn_res' AND VALUE=diagopd.cn_res) AS cn_res,(SELECT pvalue FROM masdiagop WHERE art='ataxia' AND VALUE=diagopd.ataxia) AS ataxia,(SELECT pvalue FROM masdiagop WHERE art='ftn' AND VALUE=diagopd.ftn) AS ftn,ref_l,ref_r,neu_other,other FROM (SELECT * FROM diagopd WHERE vn = '$_GET[VN]' AND drid = '$DRID') AS diagopd;";
								$resultCheckup = mysql_query($sqlGetCheckup);
								$checkupNumRow = mysql_num_rows($resultCheckup);
								
								if($checkupNumRow > 0){
									$check = false;
									$newline = false;
									while($rowCheckUp = mysql_fetch_array($resultCheckup) ){
										$GA_1 = $rowCheckUp["GA_1"];
										$GA_2 = $rowCheckUp["GA_2"];
										$GA_3 = $rowCheckUp["GA_3"];
										$GA_4 = $rowCheckUp["GA_4"];
										$GA_5 = $rowCheckUp["GA_5"];
										$GA_6 = $rowCheckUp["GA_6"];
										$GA_7 = $rowCheckUp["GA_7"];
										$hydro = $rowCheckUp["hydro"];
										$gcs = $rowCheckUp["gcs"];
										$gcs_e = $rowCheckUp["gcs_e"];
										$gcs_m = $rowCheckUp["gcs_m"];
										$gcs_v = $rowCheckUp["gcs_v"];
										$ln = $rowCheckUp["LN"];
										$eye_1 = $rowCheckUp["eye_1"];
										$ga_other = $rowCheckUp["ga_other"];
										$heent_1 = $rowCheckUp["heent_1"];
										$heent_2 = $rowCheckUp["heent_2"];
										$heent_3 = $rowCheckUp["heent_3"];
										$heent_4 = $rowCheckUp["heent_4"];
										$eye_2 = $rowCheckUp["eye_2"];
										$eye_3 = $rowCheckUp["eye_3"];
										$eye_4 = $rowCheckUp["eye_4"];
										$tonsil = $rowCheckUp["tonsil"];
										$ts_other = $rowCheckUp["ts_other"];
										$thyroid = $rowCheckUp["thyroid"];
										$tr_other = $rowCheckUp["tr_other"];
										$beat = $rowCheckUp["beat"];
										$mur = $rowCheckUp["mur"];
										$s_grade = $rowCheckUp["s_grade"];
										$s_area = $rowCheckUp["s_area"];
										$d_grade = $rowCheckUp["d_grade"];
										$d_area = $rowCheckUp["d_area"];
										$lung = $rowCheckUp["lung"];
										$l_wheez = $rowCheckUp["l_wheez"];
										$l_rhon = $rowCheckUp["l_rhon"];
										$crep_fine = $rowCheckUp["crep_fine"];
										$crep_coarse = $rowCheckUp["crep_coarse"];
										$l_other = $rowCheckUp["l_other"];
										$contour = $rowCheckUp["contour"];
										$bowel = $rowCheckUp["bowel"];
										$knock = $rowCheckUp["knock"];
										$palp = $rowCheckUp["palp"];
										$area = $rowCheckUp["area"];
										$pal_other = $rowCheckUp["pal_other"];
										$skin = $rowCheckUp["skin"];
										$skin_other = $rowCheckUp["skin_other"];
										$extrem = $rowCheckUp["extrem"];
										$ten_at = $rowCheckUp["ten_at"];
										$extre_other = $rowCheckUp["extre_other"];
										$neuro = $rowCheckUp["neuro"];
										$mg_up_l = $rowCheckUp["mg_up_l"];
										$mg_up_r = $rowCheckUp["mg_up_r"];
										$mg_lo_l = $rowCheckUp["mg_lo_l"];
										$mg_lo_r = $rowCheckUp["mg_lo_r"];
										$cn = $rowCheckUp["cn"];
										$cn_res = $rowCheckUp["cn_res"];
										$ataxia = $rowCheckUp["ataxia"];
										$ftn = $rowCheckUp["ftn"];
										$ref_l = $rowCheckUp["ref_l"];
										$ref_r = $rowCheckUp["ref_r"];
										$neu_other = $rowCheckUp["neu_other"];
										$other = $rowCheckUp["other"];
										
										if($GA_1 != ""){
											if($check == true){
												echo ", ".$GA_1;
											}else{
												echo "GA : ".$GA_1;
												$check = true;
												$newline = true;
											}
										}
										
										if($GA_2 != ""){
											if($check == true){
												echo ", ".$GA_2;
											}else{
												echo "GA : ".$GA_2;
												$check = true;
												$newline = true;
											}
										}
										
										if($GA_3 != ""){
											if($check == true){
												echo ", ".$GA_3;
											}else{
												echo "GA : ".$GA_3;
												$check = true;
												$newline = true;
											}
										}
										
										if($GA_4 != ""){
											if($check == true){
												echo ", ".$GA_4;
											}else{
												echo "GA : ".$GA_4;
												$check = true;
												$newline = true;
											}
										}
										
										if($GA_5 != ""){
											if($check == true){
												echo ", ".$GA_5;
											}else{
												echo "GA : ".$GA_5;
												$check = true;
												$newline = true;
											}
										}
										
										if($GA_6 != ""){
											if($check == true){
												echo ", ".$GA_6;
											}else{
												echo "GA : ".$GA_6;
												$check = true;
												$newline = true;
											}
										}
										
										if($GA_7 != ""){
											if($check == true){
												echo ", ".$GA_7;
											}else{
												echo "GA : ".$GA_7;
												$check = true;
												$newline = true;
											}
										}
										
										if($hydro != ""){
											if($check == true){
												echo ", Dehydration : ".$hydro;
											}else{
												echo "Dehydration : ".$hydro;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$check = false;
										$newline = false;
										
										if($gcs_e != 0){
											if($check == true){
												echo ", E : ".$gcs_e;
											}else{
												echo "GCS : E ".$gcs_e;
												$check = true;
												$newline = true;
											}
										}
										
										if($gcs_m != 0){
											if($check == true){
												echo ", M : ".$gcs_m;
											}else{
												echo "GCS : M ".$gcs_m;
												$check = true;
												$newline = true;
											}
										}
										
										if($gcs_v != 0){
											if($check == true){
												echo ", V : ".$gcs_v;
											}else{
												echo "GCS : V ".$gcs_v;
												$check = true;
												$newline = true;
											}
										}
										
										if($ga_other != ""){
											if($check == true){
												echo ", Other :".$ga_other;
											}else{
												echo "Other : ".$ga_other;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$check = false;
										$newline = false;
										
										if($heent_1 != ""){
											if($check == true){
												echo ", ".$heent_1;
											}else{
												echo "HEENT : ".$heent_1;
												$check = true;
												$newline = true;
											}
										}
										
										if($heent_2 != ""){
											if($check == true){
												echo ", ".$heent_2;
											}else{
												echo "HEENT : ".$heent_2;
												$check = true;
												$newline = true;
											}
										}
										
										if($heent_3 != ""){
											if($check == true){
												echo ", ".$heent_3;
											}else{
												echo "HEENT : ".$heent_3;
												$check = true;
												$newline = true;
											}
										}
										
										if($heent_4 != ""){
											if($check == true){
												echo ", ".$heent_4;
											}else{
												echo "HEENT : ".$heent_4;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$check = false;
										$newline = false;
										
										if($ln != ""){
											if($check == true){
												echo ", ".$ln;
											}else{
												echo "cervical LN : ".$ln;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$check = false;
										$newline = false;
										
										if($eye_1 != ""){
											if($check == true){
												echo ", ".$eye_1;
											}else{
												echo "eye : ".$eye_1;
												$check = true;
												$newline = true;
											}
										}
										
										if($eye_2 != ""){
											if($check == true){
												echo ", ".$eye_2;
											}else{
												echo "eye : ".$eye_2;
												$check = true;
												$newline = true;
											}
										}
										
										if($eye_3 != ""){
											if($check == true){
												echo ", ".$eye_3;
											}else{
												echo "eye : ".$eye_3;
												$check = true;
												$newline = true;
											}
										}
										
										if($eye_4 != ""){
											if($check == true){
												echo ", ".$eye_4;
											}else{
												echo "eye : ".$eye_4;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$check = false;
										$newline = false;
										
										if($tonsil != ""){
											if($check == true){
												echo ", ".$tonsil;
											}else{
												echo "tonsils : ".$tonsil;
												$check = true;
												$newline = true;
											}
										}
										
										if($ts_other != ""){
											if($check == true){
												echo ", Other : ".$ts_other;
											}else{
												echo "tonsils other : ".$ts_other;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$check = false;
										$newline = false;
										
										if($thyroid != ""){
											if($check == true){
												echo ", ".$thyroid;
											}else{
												echo "thyroids : ".$thyroid;
												$check = true;
												$newline = true;
											}
										}
										
										if($tr_other != ""){
											if($check == true){
												echo ", ".$tr_other;
											}else{
												echo "thyroids other : ".$tr_other;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$newline = false;
										$check = false;
										
										if($beat != ""){
											if($check == true){
												echo ", ".$beat;
											}else{
												echo "Beat : ".$beat;
												$check = true;
												$newline = true;
											}
										}
										
										if($mur != ""){
											if($check == true){
												echo ", ".$mur;
											}else{
												echo "HEART : ".$mur;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$check = false;
										$newline = false;
										
										if($s_grade != ""){
											if($check == true){
												echo ", ".$s_grade;
											}else{
												echo "systolic : ".$s_grade;
												$check = true;
												$newline = true;
											}
										}
										
										if($s_area != ""){
											if($check == true){
												echo ", Area : ".$s_area;
											}else{
												echo "systolic : ".$s_area;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$check = false;
										$newline = false;
										
										if($d_grade != ""){
											if($check == true){
												echo ", ".$d_grade;
											}else{
												echo "diastolic : ".$d_grade;
												$check = true;
												$newline = true;
											}
										}
										
										if($d_area != ""){
											if($check == true){
												echo ", Area : ".$d_area;
											}else{
												echo "diastolic : ".$d_area;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$check = false;
										$newline = false;
										
										if($lung != ""){
											if($check == true){
												echo ", ".$lung;
											}else{
												echo "Lung : ".$lung;
												$check = true;
												$newline = true;
											}
										}
										
										if($l_wheez != ""){
											if($check == true){
												echo ", ".$l_wheez;
											}else{
												echo "Lung : ".$l_wheez;
												$check = true;
												$newline = true;
											}
										}
										
										if($l_rhon != ""){
											if($check == true){
												echo ", ".$l_rhon;
											}else{
												echo "Lung : ".$l_rhon;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$check = false;
										$newline = false;
										
										if($crep_fine != ""){
											if($check == true){
												echo ", ".$crep_fine;
											}else{
												echo "Crepitation : ".$crep_fine;
												$check = true;
												$newline = true;
											}
										}
										
										if($crep_coarse != ""){
											if($check == true){
												echo ", ".$crep_coarse;
											}else{
												echo "Crepitation : ".$crep_coarse;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$check = false;
										$newline = false;
										
										if($l_other != ""){
											if($check == true){
												echo ", ".$l_other;
											}else{
												echo "Lung others : ".$l_other;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$newline = false;
										$check = false;
										
										if($contour != ""){
											if($check == true){
												echo ", ".$contour;
											}else{
												echo "Contour : ".$contour;
												$check = true;
												$newline = true;
											}
										}
										
										if($bowel != ""){
											if($check == true){
												echo ", bowel sound : ".$bowel;
											}else{
												echo "bowel sound : ".$bowel;
												$check = true;
												$newline = true;
											}
										}
										
										if($knock != ""){
											if($check == true){
												echo ", เคาะ : ".$knock;
											}else{
												echo "เคาะ : ".$knock;
												$check = true;
												$newline = true;
											}
										}
										
										if($palp != ""){
											if($check == true){
												echo ", Palpation : ".$palp;
											}else{
												echo "Palpation : ".$palp;
												$check = true;
												$newline = true;
											}
										}
										
										
										if($area != ""){
											if($check == true){
												echo ", Palpation : area : ".$area;
											}else{
												echo "Palpation area : ".$area;
												$check = true;
												$newline = true;
											}
										}
										
										if($pal_other != ""){
											if($check == true){
												echo ", Other : ".$pal_other;
											}else{
												echo "Palpation Other : ".$pal_other;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$newline = false;
										$check = false;
										
										if($skin != ""){
											if($check == true){
												echo ", ".$skin;
											}else{
												echo "Skin : ".$skin;
												$check = true;
												$newline = true;
											}
										}
										
										if($skin_other != ""){
											if($check == true){
												echo ", Other : ".$skin_other;
											}else{
												echo "Skin Other : ".$skin_other;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$newline = false;
										$check = false;
										
										if($extrem != ""){
											if($check == true){
												echo ", ".$extrem;
											}else{
												echo "Extrem : ".$extrem;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$newline = false;
										$check = false;
										
										if($ten_at != ""){
											if($check == true){
												echo ", ".$ten_at;
											}else{
												echo "Tender at : ".$ten_at;
												$check = true;
												$newline = true;
											}
										}
										
										if($extre_other != ""){
											if($check == true){
												echo ", Other : ".$extre_other;
											}else{
												echo "Extrem Others at : ".$extre_other;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$newline = false;
										$check = false;
										
										if($neuro != ""){
											if($check == true){
												echo ", ".$neuro;
											}else{
												echo "Neuro : ".$neuro;
												$check = true;
												$newline = true;
											}
										}
										
										if($mg_up_l != ""){
											if($check == true){
												echo ", ".$mg_up_l;
											}else{
												echo "Neuro Motor Grade Upper - L : ".$mg_up_l;
												$check = true;
												$newline = true;
											}
										}
										
										if($mg_up_r != ""){
											if($check == true){
												echo ", ".$mg_up_r;
											}else{
												echo "Neuro Motor Grade Upper - R : ".$mg_up_r;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$newline = false;
										$check = false;
										
										if($mg_lo_l != ""){
											if($check == true){
												echo ", ".$mg_lo_l;
											}else{
												echo "Neuro Motor Grade Lower - L : ".$mg_lo_l;
												$check = true;
												$newline = true;
											}
										}
										
										if($mg_lo_r != ""){
											if($check == true){
												echo ", ".$mg_lo_r;
											}else{
												echo "Neuro Motor Grade Lower - R : ".$mg_lo_r;
												$check = true;
												$newline = true;
											}
										}
										
										
										if($newline == true){
											echo "<br>";
										}
										
										$newline = false;
										$check = false;
										
										if($cn != 0){
											if($check == true){
												echo ", ".$cn;
											}else{
												echo "CN : ".$cn;
												$check = true;
												$newline = true;
											}
										}
										
										if($cn_res != ""){
											if($check == true){
												echo ", ".$cn_res;
											}else{
												echo "CN : ".$cn_res;
												$check = true;
												$newline = true;
											}
										}
										
										
										if($newline == true){
											echo "<br>";
										}
										
										$newline = false;
										$check = false;
										
										if($ataxia != ""){
											if($check == true){
												echo ", ".$ataxia;
											}else{
												echo "Ataxia : ".$ataxia;
												$check = true;
												$newline = true;
											}
										}
										
										if($ftn != ""){
											if($check == true){
												echo ", Neuro FTN : ".$ftn;
											}else{
												echo "Neuro FTN : ".$ftn;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$newline = false;
										$check = false;
										
										if($ref_l != ""){
											if($check == true){
												echo ", ".$ref_l;
											}else{
												echo "Refelex : ".$ref_l;
												$check = true;
												$newline = true;
											}
										}
										
										if($ref_r != ""){
											if($check == true){
												echo ", ".$ref_r;
											}else{
												echo "Refelex : ".$ref_r;
												$check = true;
												$newline = true;
											}
										}
										
										if($neu_other != ""){
											if($check == true){
												echo ", Other : ".$neu_other;
											}else{
												echo "Neuro Other : ".$neu_other;
												$check = true;
												$newline = true;
											}
										}
										
										if($newline == true){
											echo "<br>";
										}
										
										$newline = false;
										$check = false;
										
										if($other != ""){
											if($check == true){
												echo ",".$other;
											}else{
												echo "Other : ".$other;
												$check = true;
												$newline = true;
											}
										}
									}
								}
								
								//diag
								$sqlDiag = "SELECT icdcode AS 'Code', icddesc_th AS 'Disease_TH', icddesc_en AS 'Disease_EN', typediag AS 'TypeDiag', v.icdid AS 'id', v.diagdsc, mt.diagtype FROM vndiag v LEFT JOIN masdiagcode m ON v.icdid = m.icdid LEFT JOIN masdiagtype mt ON typediag = mt.diagtpeid WHERE v.vn='$_GET[VN]' AND v.`status`='1' AND v.`provider` = '$PROVIDER';";
								$diagResult = mysql_query($sqlDiag);
								$diagNumRows = mysql_num_rows($diagResult);
								
								if($diagNumRows > 0){
									echo "<div class='media'>";
										echo "<div class='media-left'>";
											//echo "<a href='#'>";
												echo "Dx";
											//echo "</a>";
										echo "</div>";
										echo "<div class='media-body'>";
							
									while($row2 = mysql_fetch_array($diagResult) ){
										$Code = $row2["Code"];
										$Disease_TH = $row2["Disease_TH"];
										$Disease_EN = $row2["Disease_EN"];
										$DIAGTYPE = $row2["diagtype"];
										$TypeDiag = $row2["TypeDiag"];
										$DiagDSC = $row2["diagdsc"];
										
										echo "<span id='sign'><strong>".$TypeDiag." : ".$Code." ".$DiagDSC." "."</strong></span>"."<br>";
										//echo "<span id='sign'><strong>".$TypeDiag." : ".$Code." ".$Disease_EN." "."</strong></span>"."<br>";
										//echo "<span id='sign'><strong>".$DIAGTYPE." ".$Code." ".$Disease_TH." ".$Disease_EN." "."</strong></span>"."<br>";
									}
									
										echo "</div>";
									echo "</div>";
									echo "<br>";
								}
								
								//$sqlAppointment = "SELECT apdate,asttime,aentime,GROUP_CONCAT(concat('- ',remarkcomment) SEPARATOR '<br>') AS 'remark' FROM frnappointment ap LEFT JOIN frnappointment_remark re ON ap.appointid = re.appointid WHERE vn_a = '$_GET[VN]' GROUP BY apdate;";
								$sqlAppointment = "SELECT apdate,asttime,aentime,re.remark FROM frnappointment ap LEFT JOIN (  SELECT GROUP_CONCAT(concat('- ',remarkcomment) SEPARATOR '<br>')   AS 'remark' , APPOINTID   FROM frnappointment_remark   GROUP BY  APPOINTID     ) re ON ap.appointid = re.appointid WHERE vn_a = '$_GET[VN]' AND DOCTOR = '$PROVIDER'";
								$result3 = mysql_query($sqlAppointment);
								$appointNumRow = mysql_num_rows($result3);
								
								if($appointNumRow > 0){
									echo "<span id='appointment'>";
									while($row3 = mysql_fetch_array($result3) ){
										$apdate = $row3["apdate"];
										$asttime = $row3["asttime"];
										$aentime = $row3["aentime"];
										$remark = $row3["remark"];
										
										echo "นัดตรวจครั้งต่อไป"."วันที่ ".convertDateBD($apdate)." [".$asttime." - ".$aentime."]"."<br>";
										echo "คำสั่งแพทย์ : "."<br>".$remark."<br>";
										echo "--------------------------------------------------------------------------------"."<br>";
									}
									echo "</span>";
								}
								
								echo "<footer>".$PROVIDERNAME." : ".$D_UPDATE."</footer></blockquote><hr>";
								//echo "<small><span id='sign'>(".$PROVIDERNAME." : ".$D_UPDATE.")</span></small>"."<br><hr>";
							}
						}
						echo "</span>";
						echo "<br><br>";
						
						//drug
						echo "<span id='drug'>";
							$sqlSearchOrder = "SELECT prdorder.ORDERID, prdorder.D_ORDER, CONCAT_WS(' ', h1.`name`, h1.lname) AS 'doctorDrug', CONCAT_WS(' ', h2.`name`, h2.lname) AS 'makerDrug', prdorder.dr_provider, prdorder.mk_provider FROM prdorder LEFT JOIN hospemp AS h1 ON h1.empid = prdorder.dr_provider LEFT JOIN hospemp AS h2 ON h2.empid = prdorder.mk_provider WHERE prdorder.vn = '$_GET[VN]' AND prdorder.hn = '$_GET[HN]' AND prdorder.f_cancel = '0'";
							$sqlSearchOrderResult = mysql_query($sqlSearchOrder);
							$sqlSearchOrderNumRow = mysql_num_rows($sqlSearchOrderResult);
							
							if($sqlSearchOrderNumRow > 0){
								echo "<p><strong>[Rx]</strong></p>";
								while($rowOrderid = mysql_fetch_array($sqlSearchOrderResult) ){
									$orderid = $rowOrderid["ORDERID"];
									$doctorDrug = $rowOrderid["doctorDrug"];
									$makerDrug = $rowOrderid["makerDrug"];
									$d_order = $rowOrderid["D_ORDER"];
									
									$sqlDrug = "SELECT b.`prdname` , a.`qty` , d.`unitname_th`, a.label FROM  prdorderdt a JOIN masproduct b ON a.prdcode = b.prdcode JOIN drugitem c ON c.prdcode = b.prdcode LEFT JOIN masunit d ON a.unitid = d.unitid WHERE b.STATUS = '1' AND a.f_cancel = '0' AND ORDERID = '$orderid' AND a.prdcat IN (1,2);";
									$drugResult = mysql_query($sqlDrug);
									$drugNumRows = mysql_num_rows($drugResult);
									
									if($drugNumRows > 0){
										
										echo "<blockquote>";
										$i = 0;
										while($rowDrug = mysql_fetch_array($drugResult) ){
											$prdname = $rowDrug["prdname"];
											$qty = $rowDrug["qty"];
											$unitname_th = $rowDrug["unitname_th"];
											$label = $rowDrug["label"];
											
											if($i != $drugNumRows - 1){
												echo $prdname.' '.$qty.' '.$unitname_th." | ".$label."<br>";
											}else{
												echo $prdname.' '.$qty.' '.$unitname_th." | ".$label;
											}
											
										}
										echo "<footer>";
										if($doctorDrug == ""){
											echo $doctorDrug;
										}else{
											echo $makerDrug;
										}
										echo ' : '.$orderid.' : '.$d_order."</footer>";
										echo "</blockquote>";
										echo "<hr>";
									}
								}
							}
						echo "</span>";
						echo "<br><br>";
						
						//nursenote
						echo "<span id='nursenote'>";
						$sqlNursenote = "SELECT a.nsnote, CONCAT_WS(' ', b.`name`, b.`lname`) AS 'PROVIDERNAME',a.nndt FROM vnnote a LEFT JOIN hospemp b ON a.ns = b.empid WHERE a.vn = '$_GET[VN]' AND a.`status` = '1'";
						$resultNursenote = mysql_query($sqlNursenote);
						$nursenoteNumRows = mysql_num_rows($resultNursenote);
						
						if($nursenoteNumRows > 0){
							echo "<p><strong>[Nurse]</strong></p>";
							while($row = mysql_fetch_array($resultNursenote) ){
								$NSNOTE = $row["nsnote"];
								$PROVIDERNAME = $row["PROVIDERNAME"];
								$NNDT = $row["nndt"];
								
								echo "<blockquote>";
								echo $NSNOTE;
								//echo "<br>";
								echo "<footer>".$PROVIDERNAME." : ".$NNDT."</footer>";
								echo "</blockquote>";
								echo "<hr>";
							}
							
						}
						echo "</span>";
						
						mysql_close();
					?>
				</div>
			</div>
		</div>
		
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script src="ckeditor/ckeditor.js"></script>
	<script>
		// Replace the <textarea id="editor1"> with a CKEditor
		// instance, using default configuration.
		//CKEDITOR.replace( 'drnote', {
		//	height: '355',
		//	uiColor: '#9AB8F3'
		//});
			
		//CKEDITOR.config.toolbar = [
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'BGColor', 'Image', 'Print' ] }
			//{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
			//{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
			//{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
			//{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
			//'/',
			//{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
			//{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
			//{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
			//{ name: 'insert', items: [ 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
			//{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
			//'/',
			//{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
			//{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			//{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
			//{ name: 'others', items: [ '-' ] },
			//{ name: 'about', items: [ 'About' ] }
		];
	</script>
	</body>
</html>