<!DOCTYPE html>
<html lang="en">
	<head>
		<?php
			include "include/connectdb.php";
			
			function convertDateBD($date){
				//$dateTimeExplode = explode(" ", $dateTime);
				//$date = $dateTimeExplode[0];
				//$time = $dateTimeExplode[1];

				$dateExplode = explode('-', $date);
				//$timeExplode = explode(':', $time);

				//convert year
				$bdYear = $dateExplode[0] + 543;
				//convert month
				switch ($dateExplode[1]) {
					case 01:
						$bdMonth = 'มกราคม';
						break;
					case 02:
						$bdMonth = 'กุมภาพันธ์';
						break;
					case 03:
						$bdMonth = 'มีนาคม';
						break;
					case 04:
						$bdMonth = 'เมษายน';
						break;
					case 05:
						$bdMonth = 'พฤษภาคม';
						break;
					case 06:
						$bdMonth = 'มิถุนายน';
						break;
					case 07:
						$bdMonth = 'กรกฎาคม';
						break;
					case 08:
						$bdMonth = 'สิงหาคม';
						break;
					case 09:
						$bdMonth = 'กันยายน';
						break;
					case 10:
						$bdMonth = 'ตุลาคม';
						break;
					case 11:
						$bdMonth = 'พฤศจิกายน';
						break;
					case 12:
						$bdMonth = 'ธันวาคม';
						break;
				}
				$bdDate = $dateExplode[2];
				//$time = $time;
				
				return $bdDate." ".$bdMonth." ".$bdYear;
			}
		?>
		<meta charset="utf-8">
		<!-- Clear Cache -->
		<!--
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
		-->
		<!--End of Clear Cache -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>FRIENDS's View Dr.Note</title>

		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<style>
			#drnote{
				color: navy;
			}
			
			#nursenote{
				color: #ce2f00;
			}
			
			#drug{
				color: olive;
			}
			
			#sign{
				color: black;
			}
			
			hr{
				border: 0;
				height: 1px;
				background: #333;
				background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
				background-image:    -moz-linear-gradient(left, #ccc, #333, #ccc);
				background-image:     -ms-linear-gradient(left, #ccc, #333, #ccc);
				background-image:      -o-linear-gradient(left, #ccc, #333, #ccc);
			}
			
			blockquote{
				font-size: 14px;
			}
			
			span#appointment{
				color: #CF0000;
			}
		</style>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="js/html5shiv.min.js"></script>
		  <script src="js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-sm-12'>
					<?php
						//vitalsign
						$sqlVitalSign = "SELECT opd.VSID,RESULTVS,VSDESC,LL,HL,p.NATION,mna.NATIONDESC,fl.FEELING,pc.TELEPHONE AS TELEPHONEPC,pc.MOBILE AS MOBILEPC,dru.DRUGALLERGY AS 'แพ้ยา',dst.DISEASE AS 'โรคประจำตัว', se.CHIEFCOMP FROM (SELECT * FROM myfriendsdb.frnvsopd WHERE(VN = '$_GET[VN]') ) AS opd LEFT JOIN  (SELECT * FROM myfriendsdb.masvstype WHERE(`STATUS` = 1)) AS vst on opd.VSID = vst.VSID LEFT JOIN myfriendsdb.frnservice AS se ON opd.VN = se.VN LEFT JOIN myfriendsdb.masfeeling AS fl ON se.FEELID = fl.FEELID LEFT JOIN (SELECT * FROM myfriendsdb.person WHERE(HN = '$_GET[HN]' AND `STATUS` = 1))  AS p ON se.HN = p.HN LEFT JOIN (SELECT * FROM myfriendsdb.masnation WHERE(`STATUS` = 1)) AS mna ON p.NATION = mna.NATION LEFT JOIN (SELECT * FROM myfriendsdb.personcontact) AS pc ON p.HN = pc.HN LEFT JOIN (SELECT HN, GROUP_CONCAT(DRUGALLERGY SEPARATOR ',') AS DRUGALLERGY FROM myfriendsdb.drugallergy GROUP BY HN) AS dru ON p.HN = dru.HN LEFT JOIN (SELECT HN, GROUP_CONCAT(DISEASE SEPARATOR ',') AS DISEASE FROM myfriendsdb.condisease WHERE(`STATUS` = 1) GROUP BY HN) AS dst ON p.HN = dst.HN";
						$resultVitalsign = mysql_query($sqlVitalSign);
						$vsNumRows = mysql_num_rows($resultVitalsign);
						
						echo "<br>";
						echo "<strong>";
						if($vsNumRows > 0){
							while($rowVS = mysql_fetch_array($resultVitalsign) ){
								$VSDESC = $rowVS["VSDESC"];
								$RESULTVS = $rowVS["RESULTVS"];
								$CHIEFCOMP = $rowVS["CHIEFCOMP"];
								
								if($VSDESC == "Systolic"){
									$VSDESCBPS = $rowVS["VSDESC"];
									$BPS = $RESULTVS;
								}elseif($VSDESC == "Diastolic"){
									$VSDESCBPD = $rowVS["VSDESC"];
									$BPD = $RESULTVS;
								}else{
									if($VSDESC == "Pulse rate" OR $VSDESC == "อัตราการหายใจ"){
										echo $VSDESC." : ".number_format($RESULTVS, 0)." | ";
									}else{
										echo $VSDESC." : ".$RESULTVS." | ";
									}
								}
								
								/*
								if($VSDESC == "BP (Systolic)"){
									$VSDESCBPS = $rowVS["VSDESC"];
									$BPS = $RESULTVS;
								}elseif($VSDESC == "BP (Diastolic)"){
									$VSDESCBPD = $rowVS["VSDESC"];
									$BPD = $RESULTVS;
								}else{
									echo $VSDESC." : ".$RESULTVS." | ";
								}
								*/
							}
							
							if(isset($BPS) AND isset($BPD)){
								echo "BP : ".number_format($BPS, 0)." / ".number_format($BPD, 0);
							}
							echo "<br><br>";
							echo 'CC: '.$CHIEFCOMP;
						}
						echo "</strong>";
						echo "<br><br>";
						
						//drnote
						echo "<span id='drnote'>";
						$sqlDrnote = "SELECT a.`VN` AS 'VN', a.`DRNOTE` AS 'DRNOTE', a.`PROVIDER` AS 'PROVIDER', CONCAT_WS(' ', b.`NAME`, b.`LNAME`) AS 'PROVIDERNAME', a.`D_UPDATE` AS 'D_UPDATE' FROM diagopd a LEFT JOIN hospemp b ON a.PROVIDER = b.EMPID WHERE a.`VN` = '$_GET[VN]' AND a.`STATUS` = '1' ORDER BY a.`D_UPDATE` DESC";
						$resultsqlDrnote = mysql_query($sqlDrnote);
						$drnoteNumRows = mysql_num_rows($resultsqlDrnote);
						
						if($drnoteNumRows > 0){
							echo "<p><strong>[Doctor]</strong></p>";
							while($row = mysql_fetch_array($resultsqlDrnote) ){
								$VN = $row["VN"];
								$DRNOTE = $row["DRNOTE"];
								$PROVIDER = $row["PROVIDER"];
								$PROVIDERNAME = $row["PROVIDERNAME"];
								$D_UPDATE = $row["D_UPDATE"];
								
								echo "<blockquote>";
								echo $DRNOTE;
								
								//diag
								$sqlDiag = "SELECT icdcode AS 'Code',icddesc_th AS 'Disease_TH',icddesc_en AS 'Disease_EN',typediag AS 'TypeDiag',v.icdid AS 'id', mt.DIAGTYPE FROM myfriendsdb.vndiag v JOIN myfriendsdb.masdiagcode m ON v.icdid = m.icdid JOIN myfriendsdb.masdiagtype mt ON typediag = mt.DIAGTPEID WHERE v.vn='$_GET[VN]' AND v.STATUS='1' AND v.PROVIDER = '$PROVIDER';";
								$diagResult = mysql_query($sqlDiag);
								$diagNumRows = mysql_num_rows($diagResult);
								
								if($diagNumRows > 0){
									echo "<div class='media'>";
										echo "<div class='media-left'>";
											//echo "<a href='#'>";
												echo "Dx";
											//echo "</a>";
										echo "</div>";
										echo "<div class='media-body'>";
							
									while($row2 = mysql_fetch_array($diagResult) ){
										$Code = $row2["Code"];
										$Disease_TH = $row2["Disease_TH"];
										$Disease_EN = $row2["Disease_EN"];
										$DIAGTYPE = $row2["DIAGTYPE"];
										$TypeDiag = $row2["TypeDiag"];
										
										echo "<span id='sign'><strong>".$TypeDiag." : ".$Code." ".$Disease_EN." "."</strong></span>"."<br>";
										//echo "<span id='sign'><strong>".$DIAGTYPE." ".$Code." ".$Disease_TH." ".$Disease_EN." "."</strong></span>"."<br>";
									}
									
										echo "</div>";
									echo "</div>";
									echo "<br>";
								}
								
								//$sqlAppointment = "SELECT apdate,asttime,aentime,GROUP_CONCAT(concat('- ',remarkcomment) SEPARATOR '<br>') AS 'remark' FROM myfriendsdb.frnappointment ap LEFT JOIN myfriendsdb.frnappointment_remark re ON ap.appointid = re.appointid WHERE vn_a = '$_GET[VN]' GROUP BY apdate;";
								$sqlAppointment = "SELECT apdate,asttime,aentime,re.remark FROM myfriendsdb.frnappointment ap LEFT JOIN (  SELECT GROUP_CONCAT(concat('- ',remarkcomment) SEPARATOR '<br>')   AS 'remark' , APPOINTID   FROM myfriendsdb.frnappointment_remark   GROUP BY  APPOINTID     ) re ON ap.appointid = re.appointid WHERE vn_a = '$_GET[VN]' AND DOCTOR = '$PROVIDER'";
								$result3 = mysql_query($sqlAppointment);
								$appointNumRow = mysql_num_rows($result3);
								
								if($appointNumRow > 0){
									echo "<span id='appointment'>";
									while($row3 = mysql_fetch_array($result3) ){
										$apdate = $row3["apdate"];
										$asttime = $row3["asttime"];
										$aentime = $row3["aentime"];
										$remark = $row3["remark"];
										
										echo "นัดตรวจครั้งต่อไป"."วันที่ ".convertDateBD($apdate)." [".$asttime." - ".$aentime."]"."<br>";
										echo "คำสั่งแพทย์ : "."<br>".$remark."<br>";
										echo "--------------------------------------------------------------------------------"."<br>";
									}
									echo "</span>";
								}
								
								echo "<footer>".$PROVIDERNAME." : ".$D_UPDATE."</footer></blockquote><hr>";
								//echo "<small><span id='sign'>(".$PROVIDERNAME." : ".$D_UPDATE.")</span></small>"."<br><hr>";
							}
						}
						echo "</span>";
						echo "<br><br>";
						
						//drug
						echo "<span id='drug'>";
							$sqlSearchOrder = "SELECT prdorder.ORDERID, prdorder.D_ORDER, CONCAT_WS(' ', h1.`name`, h1.lname) AS 'doctorDrug', CONCAT_WS(' ', h2.`name`, h2.lname) AS 'makerDrug', prdorder.dr_provider, prdorder.mk_provider FROM prdorder JOIN hospemp AS h1 ON h1.empid = prdorder.dr_provider JOIN hospemp AS h2 ON h2.empid = prdorder.mk_provider WHERE prdorder.vn = '$_GET[VN]' AND prdorder.hn = '$_GET[HN]' AND prdorder.prdcat = '1'";
							$sqlSearchOrderResult = mysql_query($sqlSearchOrder);
							$sqlSearchOrderNumRow = mysql_num_rows($sqlSearchOrderResult);
							
							if($sqlSearchOrderNumRow > 0){
								echo "<p><strong>[Rx]</strong></p>";
								while($rowOrderid = mysql_fetch_array($sqlSearchOrderResult) ){
									$orderid = $rowOrderid["ORDERID"];
									$doctorDrug = $rowOrderid["doctorDrug"];
									$makerDrug = $rowOrderid["makerDrug"];
									$d_order = $rowOrderid["D_ORDER"];
									
									$sqlDrug = "SELECT b.`prdname` , a.`qty` , d.`unitname_th` FROM  prdorderdt a JOIN masproduct b ON a.prdcode = b.prdcode JOIN drugitem c ON c.prdcode = b.prdcode LEFT JOIN masunit d ON a.unitid = d.unitid WHERE b.STATUS = '1' AND a.f_cancel = '0' AND ORDERID = '$orderid';";
									$drugResult = mysql_query($sqlDrug);
									$drugNumRows = mysql_num_rows($drugResult);
									
									if($drugNumRows > 0){
										echo "<blockquote>";
										$i = 0;
										while($rowDrug = mysql_fetch_array($drugResult) ){
											$prdname = $rowDrug["prdname"];
											$qty = $rowDrug["qty"];
											$unitname_th = $rowDrug["unitname_th"];
											
											if($i != $drugNumRows - 1){
												echo $prdname.' '.$qty.' '.$unitname_th."<br>";
											}else{
												echo $prdname.' '.$qty.' '.$unitname_th;
											}
											
										}
										echo "<footer>";
										if($doctorDrug == ""){
											echo $doctorDrug;
										}else{
											echo $makerDrug;
										}
										echo ' : '.$orderid.' : '.$d_order."</footer>";
										echo "</blockquote>";
										echo "<hr>";
									}
								}
							}
						echo "</span>";
						echo "<br><br>";
						
						//nursenote
						echo "<span id='nursenote'>";
						$sqlNursenote = "SELECT a.nsnote, CONCAT_WS(' ', b.`name`, b.`lname`) AS 'PROVIDERNAME',a.nndt FROM vnnote a LEFT JOIN hospemp b ON a.ns = b.empid WHERE a.vn = '$_GET[VN]' AND a.`status` = '1'";
						$resultNursenote = mysql_query($sqlNursenote);
						$nursenoteNumRows = mysql_num_rows($resultNursenote);
						
						if($nursenoteNumRows > 0){
							echo "<p><strong>[Nurse]</strong></p>";
							while($row = mysql_fetch_array($resultNursenote) ){
								$NSNOTE = $row["nsnote"];
								$PROVIDERNAME = $row["PROVIDERNAME"];
								$NNDT = $row["nndt"];
								
								echo "<blockquote>";
								echo $NSNOTE;
								//echo "<br>";
								echo "<footer>".$PROVIDERNAME." : ".$NNDT."</footer>";
								echo "</blockquote>";
								echo "<hr>";
							}
							
						}
						echo "</span>";
					?>
				</div>
			</div>
		</div>
		
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script src="ckeditor/ckeditor.js"></script>
	<script>
		// Replace the <textarea id="editor1"> with a CKEditor
		// instance, using default configuration.
		//CKEDITOR.replace( 'drnote', {
		//	height: '355',
		//	uiColor: '#9AB8F3'
		//});
			
		//CKEDITOR.config.toolbar = [
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'BGColor', 'Image', 'Print' ] }
			//{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
			//{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
			//{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
			//{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
			//'/',
			//{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
			//{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
			//{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
			//{ name: 'insert', items: [ 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
			//{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
			//'/',
			//{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
			//{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			//{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
			//{ name: 'others', items: [ '-' ] },
			//{ name: 'about', items: [ 'About' ] }
		];
	</script>
	</body>
</html>