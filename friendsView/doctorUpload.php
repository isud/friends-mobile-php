<!DOCTYPE html>
<html lang="en">
	<head>
		<?php
			include "include/connectdb.php";

			function uploadPic($tmpFile, $picName, $hn){
				//random File Name
				$randomBannerName = mt_rand(1, 99);
				
				//file type
				$subFileType = explode(".", $picName);
				$fileTypeFromName = $subFileType[1];
				
				$newPicName = date('Y-m-d-H-i-s').'num'.$hn.'ran'.$randomBannerName.'rw.'.$fileTypeFromName;
				//echo "../FRI/".$hn."/".$newPicName;
				if(move_uploaded_file($tmpFile, "../FRI/".$hn."/img/caseDrawing/".$newPicName)){
					return $newPicName;
				}
			}
		?>
		<meta charset="utf-8">
		<!-- Clear Cache -->
		<!--
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
		-->
		<!--End of Clear Cache -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>FRIENDS's Dr.Note</title>

		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="js/html5shiv.min.js"></script>
		  <script src="js/respond.min.js"></script>
		<![endif]-->
		
		<style>
/*
			body{
				line-height: 0;
			}
			
			.btn-primary{
				background-color: #fff;
				border-color: #fff;
			}
			
			.btn-primary:hover, .btn-primary:focus, .btn-primary.focus, .btn-primary:active, .btn-primary.active, .open>.dropdown-toggle.btn-primary {
			  color: #fff;
			  background-color: #fff;
			  border-color: #fff;
			}
			
			.btn {
				display: inline-block;
				margin-bottom: 0;
				font-weight: normal;
				text-align: center;
				vertical-align: middle;
				-ms-touch-action: manipulation;
				touch-action: manipulation;
				cursor: pointer;
				background-image: none;
				border: 0px solid transparent;
				white-space: nowrap;
				padding: 0px 0px;
				font-size: 0px;
				line-height: 0;
				border-radius: 0px;
				-webkit-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
			}
*/
		</style>
	</head>
	<body>
		<div class='container-fluid'>
			<div class='row'>
				<form class='form-horizontal' method='post' action='' enctype='multipart/form-data'>
<div class='form-group'>					
<label for="inputEmail3" class="col-sm-3 control-label caption">upload </label>
					<div class="col-sm-9">
						<input type="file" name='attachPic'/>
					</div>
</div>

<div class='form-group'>
					<label for="inputEmail3" class="col-sm-3 control-label caption">hn </label>
					<div class="col-sm-9">
						<input type="text" name='hn'/>
					</div>
</div>

<div class='form-group'>

					<label for="inputEmail3" class="col-sm-3 control-label caption"></label>
					<div class="col-sm-9">
						<button type="submit" class='btn btn-default' name='upload'>upload</button>
					</div>
</div>
					
<?php
					if(isset($_POST['upload'])){
						if(!empty($_FILES["attachPic"]["name"])){
							$picName = uploadPic($_FILES["attachPic"]["tmp_name"], $_FILES["attachPic"]["name"], $_POST['hn']);
							//$sql1 .= "img_path, ";
							//$sql2 .= "'$picName', ";
						}
					}
					?>
				</form>
			</div>
		</div>
		 
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    		<script src="js/jquery.min.js"></script>
    		<!-- Include all compiled plugins (below), or include individual files as needed -->
    		<script src="js/bootstrap.min.js"></script>
	</body>
</html>