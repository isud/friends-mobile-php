<!DOCTYPE html>
<html lang="en">
	<head>
		<?php
			include "include/connectdb.php";
			
			function convertDateBD($date){
				//$dateTimeExplode = explode(" ", $dateTime);
				//$date = $dateTimeExplode[0];
				//$time = $dateTimeExplode[1];

				$dateExplode = explode('-', $date);
				//$timeExplode = explode(':', $time);

				//convert year
				$bdYear = $dateExplode[0] + 543;
				//convert month
				switch ($dateExplode[1]) {
					case 01:
						$bdMonth = 'มกราคม';
						break;
					case 02:
						$bdMonth = 'กุมภาพันธ์';
						break;
					case 03:
						$bdMonth = 'มีนาคม';
						break;
					case 04:
						$bdMonth = 'เมษายน';
						break;
					case 05:
						$bdMonth = 'พฤษภาคม';
						break;
					case 06:
						$bdMonth = 'มิถุนายน';
						break;
					case 07:
						$bdMonth = 'กรกฎาคม';
						break;
					case 08:
						$bdMonth = 'สิงหาคม';
						break;
					case 09:
						$bdMonth = 'กันยายน';
						break;
					case 10:
						$bdMonth = 'ตุลาคม';
						break;
					case 11:
						$bdMonth = 'พฤศจิกายน';
						break;
					case 12:
						$bdMonth = 'ธันวาคม';
						break;
				}
				$bdDate = $dateExplode[2];
				//$time = $time;
				
				return $bdDate." ".$bdMonth." ".$bdYear;
			}
		?>
		<meta charset="utf-8">
		<!-- Clear Cache -->
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
		<!--End of Clear Cache -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>FRIENDS's View Dr.Note</title>

		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<link rel="stylesheet" href="css/exampleMorris.css">
		<link rel="stylesheet" href="css/prettify.min.css">
		<link rel="stylesheet" href="css/morris.css">
		<style>
			#drnote{
				color: navy;
			}
			
			#nursenote{
				color: #ce2f00;
			}
			
			#drug{
				color: olive;
			}
			
			#sign{
				color: black;
			}
			
			hr{
				border: 0;
				height: 1px;
				background: #333;
				background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
				background-image:    -moz-linear-gradient(left, #ccc, #333, #ccc);
				background-image:     -ms-linear-gradient(left, #ccc, #333, #ccc);
				background-image:      -o-linear-gradient(left, #ccc, #333, #ccc);
			}
			
			blockquote{
				font-size: 14px;
			}
			
			span#appointment{
				color: #CF0000;
			}
			
			pre{
				font-size: 14px;
			}
			
			pre{
				color: navy;
				font-size: 100%;
				font: inherit;
				padding: 0px;
				background-color: #fff;
				border: none;
				margin: 0px 0px 0px 0px;
			}

			.red{
				color: red !important;
			}

			 svg{
				width: 100% !important;
			}
		</style>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="js/html5shiv.min.js"></script>
		  <script src="js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		
		<div class='container-fluid'>

			<!-- if have ipd data show button -->
			<?php
				$sqlCheckVN = "SELECT vn FROM frnadmission WHERE an = '$_GET[AN]'";
				$resultCheckVN = mysql_query($sqlCheckVN);
				$numRowsCheckVN = mysql_num_rows($resultCheckVN);

				if($numRowsCheckVN != 0){
					while($rowCheckVN = mysql_fetch_array($resultCheckVN) ){
						$patVN = $rowCheckVN["vn"];
					}
					echo "<div class='row'>";
						echo "<div class='col-sm-6'>";
							echo "<a class='btn btn-primary' href='viewDrNote.php?HN=".$_GET['HN'].'&VN='.$patVN."'>";
								echo 'ดูรายการ OPD';
							echo "</a>";
						echo "</div>";
					echo "</div>";
				}
			?>
			
			<br />

			<div class='row'>
				<div class='col-sm-12'>

					<!--Vital Sign-->
					<?php
						//-------------vitalsign-------------------
						//$sqlVitalSign = "SELECT frnvsopd.vsid, masvstype.vsdesc, frnvsopd.`resultvs`, masvstype.`unit`, frnvsopd.`d_update` FROM (SELECT * FROM frnvsopd WHERE an = '') AS frnvsopd JOIN masvstype ON masvstype.`vsid` = frnvsopd.`vsid` ";
						//$resultVitalSign = mysql_query($sqlVitalSign);

						echo "<div>";
							echo "<ul class='nav nav-tabs' role='tablist'>";

								$sqlGetAllVSType = "SELECT vsdata.vsid, vsdata.vsdesc, vsdata.lstorder FROM (SELECT frnvsopd.vsid, masvstype.vsdesc, frnvsopd.`resultvs`, masvstype.`unit`, frnvsopd.`d_update`, masvstype.`lstorder` FROM (SELECT * FROM frnvsopd WHERE an = '5912140001') AS frnvsopd JOIN masvstype ON masvstype.`vsid` = frnvsopd.`vsid`) AS vsdata GROUP BY vsdata.vsdesc ORDER BY vsdata.lstorder ASC";
								$resultGetAllVSType = mysql_query($sqlGetAllVSType);

								$flagAcive = 0;
								while($rowAllVSType = mysql_fetch_array($resultGetAllVSType) ){
									$vsdesc = str_replace(' ', '', $rowAllVSType["vsdesc"]);
									$vsdesc = str_replace('(', '', $vsdesc);
									$vsdesc = str_replace(')', '', $vsdesc);

									if($flagAcive == 0){
										echo "<li role='vs' class='active'><a href='#".$vsdesc."' aria-controls='".$vsdesc."' role='tab' data-toggle='tab'>".$rowAllVSType["vsdesc"]."</a></li>";
									}else{
										echo "<li role='vs'><a href='#".$vsdesc."' aria-controls='".$vsdesc."' role='tab' data-toggle='tab'>".$rowAllVSType["vsdesc"]."</a></li>";
									}
									
									$flagAcive++;
								}

							echo "</ul>";

							//<!-- Tab panes -->
							echo "<div class='tab-content'>";
								$flagAcive = 0;
								$resultGetAllVSType = mysql_query($sqlGetAllVSType);
								while($rowAllVSType = mysql_fetch_array($resultGetAllVSType) ){
									$vsid = $rowAllVSType["vsid"];
									$vsdesc = str_replace(' ', '', $rowAllVSType["vsdesc"]);
									$vsdesc = str_replace('(', '', $vsdesc);
									$vsdesc = str_replace(')', '', $vsdesc);

									if($flagAcive == 0){
										echo "<div role='".$vsdesc."' class='tab-pane active' id='".$vsdesc."'>"."<div id='".$vsid."'></div>"."</div>";
									}else{
										echo "<div role='".$vsdesc."' class='tab-pane' id='".$vsdesc."'>"."<div id='".$vsid."'></div>"."</div>";
									}

									$flagAcive++;
								}
							echo "</div>";
						echo "</div>";
						//---------------end of vitalsign-------------------------
					?>
					
					<br />
					<br />			

					<!--drug-->
					<?php
						//-------------drug---------------
						//drug
						echo "<span id='drug'>";
							$sqlSearchOrder = "SELECT prdorderdt.orderid, prdorder.d_order, prdorder.`dr_provider`, hospempDr.drName, prdorder.`mk_provider`, CONCAT_WS(' ', masprename.ftprename,hospempMk.mkName) AS 'mkName' FROM (SELECT orderid, id FROM prdorderdt WHERE f_cancel = '0' AND f_rtn = '0' AND prdcat IN (1,2)) AS prdorderdt JOIN (SELECT orderid, d_order, dr_provider, mk_provider FROM prdorder WHERE an = '$_GET[AN]') AS prdorder ON prdorder.orderid = prdorderdt.orderid LEFT JOIN (SELECT empid, CONCAT(IFNULL(`name`, ''), ' ', IFNULL(`lname`, '')) AS 'drName' FROM hospemp) AS hospempDr ON hospempDr.empid = prdorder.`dr_provider` LEFT JOIN (SELECT empid, CONCAT(IFNULL(`name`, ''), ' ', IFNULL(`lname`, '')) AS 'mkName', prename FROM hospemp) AS hospempMk ON hospempMk.empid = prdorder.`mk_provider` LEFT JOIN masprename ON masprename.prename = hospempMk.prename GROUP BY prdorderdt.orderid ORDER BY prdorder.`d_order` DESC";
							//$sqlSearchOrder = "SELECT prdorder.ORDERID, prdorder.D_ORDER, CONCAT_WS(' ', h1.`name`, h1.lname) AS 'doctorDrug', CONCAT_WS(' ', h2.`name`, h2.lname) AS 'makerDrug', prdorder.dr_provider, prdorder.mk_provider FROM prdorder LEFT JOIN hospemp AS h1 ON h1.empid = prdorder.dr_provider LEFT JOIN hospemp AS h2 ON h2.empid = prdorder.mk_provider WHERE prdorder.vn = '$_GET[VN]' AND prdorder.hn = '$_GET[HN]' AND prdorder.f_cancel = '0'";
							$sqlSearchOrderResult = mysql_query($sqlSearchOrder);
							$sqlSearchOrderNumRow = mysql_num_rows($sqlSearchOrderResult);
							
							if($sqlSearchOrderNumRow > 0){
								echo "<p><strong>[Rx]</strong></p>";
								while($rowOrderid = mysql_fetch_array($sqlSearchOrderResult) ){
									$orderid = $rowOrderid["orderid"];
									$doctorDrug = $rowOrderid["drName"];
									$makerDrug = $rowOrderid["mkName"];
									$d_order = $rowOrderid["d_order"];
									
									$sqlDrug = "SELECT b.`prdname` , a.`qty` , d.`unitname_th`, a.label FROM  prdorderdt a JOIN masproduct b ON a.prdcode = b.prdcode JOIN drugitem c ON c.prdcode = b.prdcode LEFT JOIN masunit d ON a.unitid = d.unitid WHERE b.STATUS = '1' AND a.f_cancel = '0' AND ORDERID = '$orderid' AND a.prdcat IN (1,2);";
									$drugResult = mysql_query($sqlDrug);
									$drugNumRows = mysql_num_rows($drugResult);
									
									if($drugNumRows > 0){
										
										echo "<blockquote>";
										$i = 0;
										while($rowDrug = mysql_fetch_array($drugResult) ){
											$prdname = $rowDrug["prdname"];
											$qty = $rowDrug["qty"];
											$unitname_th = $rowDrug["unitname_th"];
											$label = $rowDrug["label"];
											
											if($i != $drugNumRows - 1){
												echo $prdname.' '.$qty.' '.$unitname_th." | ".$label."<br>";
											}else{
												echo $prdname.' '.$qty.' '.$unitname_th." | ".$label;
											}
											
										}
										echo "<footer>";
										if($doctorDrug == ""){
											echo $doctorDrug;
										}else{
											echo $makerDrug;
										}
										echo ' : '.$orderid.' : '.$d_order."</footer>";
										echo "</blockquote>";
										echo "<hr>";
									}
								}
							}
						echo "</span>";
						echo "<br><br>";
						
						//nursenote
						echo "<span id='nursenote'>";
						$sqlNursenote = "SELECT a.nsnote, CONCAT_WS(' ', b.`name`, b.`lname`) AS 'PROVIDERNAME',a.nndt FROM vnnote a LEFT JOIN hospemp b ON a.ns = b.empid WHERE a.an = '$_GET[AN]' AND a.`status` = '1'";
						$resultNursenote = mysql_query($sqlNursenote);
						$nursenoteNumRows = mysql_num_rows($resultNursenote);
						
						if($nursenoteNumRows > 0){
							echo "<p><strong>[Nurse]</strong></p>";
							while($row = mysql_fetch_array($resultNursenote) ){
								$NSNOTE = $row["nsnote"];
								$PROVIDERNAME = $row["PROVIDERNAME"];
								$NNDT = $row["nndt"];
								
								echo "<blockquote>";
								echo $NSNOTE;
								//echo "<br>";
								echo "<footer>".$PROVIDERNAME." : ".$NNDT."</footer>";
								echo "</blockquote>";
								echo "<hr>";
							}
							
						}
						echo "</span>";
					?>

					
				</div>
			</div>
		</div>
		
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script src="ckeditor/ckeditor.js"></script>

	<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>-->
  	<script src="js/raphael-min.js"></script>
  	<script src="js/morris.min.js"></script>
  	<script src="js/prettify.min.js"></script>
  	<script src="js/exampleMorris.js"></script>
  
	<script>

		<?php
			//get vitasign group
			$sqlGetAllVstypeJson = "SELECT vsdata.vsid, vsdata.vsdesc FROM (SELECT frnvsopd.vsid, masvstype.vsdesc, frnvsopd.`resultvs`, masvstype.`unit`, frnvsopd.`d_update` FROM (SELECT * FROM frnvsopd WHERE an = '5912140001') AS frnvsopd JOIN masvstype ON masvstype.`vsid` = frnvsopd.`vsid`) AS vsdata GROUP BY vsdata.vsid";
			$resultGetAllVstypeJson = mysql_query($sqlGetAllVstypeJson);

			while($rowGetAllVstypeJson = mysql_fetch_array($resultGetAllVstypeJson) ){
				$vsid = $rowGetAllVstypeJson["vsid"];
				$vsdesc = str_replace(' ', '', $rowGetAllVstypeJson["vsdesc"]);
				$vsdesc = str_replace('(', '', $vsdesc);
				$vsdesc = str_replace(')', '', $vsdesc);

				//get data vitalsign
				$sqlGetDataVS = "SELECT frnvsopd.vsid, masvstype.vsdesc, frnvsopd.`resultvs`, masvstype.`unit`, frnvsopd.`d_update` FROM (SELECT * FROM frnvsopd WHERE an = '$_GET[AN]' AND vsid = '$vsid') AS frnvsopd JOIN masvstype ON masvstype.`vsid` = frnvsopd.`vsid`";
				$resultGetDataVS = mysql_query($sqlGetDataVS);
				
				$arrResultvs = array();
				
				while($rowGetDataVS = mysql_fetch_array($resultGetDataVS, MYSQL_ASSOC)){
					$row_array['resultvs'] = floatval($rowGetDataVS['resultvs']);
					$row_array['unit'] = $rowGetDataVS['unit'];
					$row_array['d_update'] = $rowGetDataVS['d_update'];
					
					array_push($arrResultvs, $row_array);
				}
				
				echo "var ".$vsdesc." = ".json_encode($arrResultvs).";"."\n\n";
				
				echo "var ".$vsdesc.$vsid." = "."Morris.Line({
element: '".$vsid."',
data: ".$vsdesc.",
xkey: 'd_update',
ykeys: ['resultvs'],
labels: ['Result'],
parseTime: false,
resize: true
});"."\n\n";

				echo "$('ul.nav a').on('shown.bs.tab', function (e) {".$vsdesc.$vsid.".redraw();});"."\n\n";
				
			}
		?>
	</script>
	</body>
</html>

<!--closecon-->
<?php
	mysql_close();
?>