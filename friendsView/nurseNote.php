<!DOCTYPE html>
<html lang="en">
	<head>
		<?php
			include "include/connectdb.php";
		?>
		<meta charset="utf-8">
		<!-- Clear Cache -->
		<!--
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
		-->
		<!--End of Clear Cache -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>FRIENDS's Nurse Note</title>

		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="js/html5shiv.min.js"></script>
		  <script src="js/respond.min.js"></script>
		<![endif]-->
		
		<style>
			body{
				line-height: 0;
			}
			
			.btn-warning{
				background-color: #fff;
				border-color: #fff;
			}
			
			.btn-warning:hover, .btn-warning:focus, .btn-warning.focus, .btn-warning:active, .btn-warning.active, .open>.dropdown-toggle.btn-warning {
			  color: #fff;
			  background-color: #fff;
			  border-color: #fff;
			}
			
			.btn {
				display: inline-block;
				margin-bottom: 0;
				font-weight: normal;
				text-align: center;
				vertical-align: middle;
				-ms-touch-action: manipulation;
				touch-action: manipulation;
				cursor: pointer;
				background-image: none;
				border: 0px solid transparent;
				white-space: nowrap;
				padding: 0px 0px;
				font-size: 0px;
				line-height: 0;
				border-radius: 0px;
				-webkit-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
			}
		</style>
	</head>
	<body>
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-sm-12' style='padding-left: 0px; padding-right: 0px;'>
					<form method='post' action=''>
						<textarea class="form-control" id='nursenote' rows="20" name='nursenote'>
							<?php
								$sql = "SELECT a.`VN` AS 'VN', a.`NSNOTE` AS 'NSNOTE', a.`NS` AS 'PROVIDER', CONCAT_WS(' ', b.`NAME`, b.`LNAME`) AS 'PROVIDERNAME', a.`NNDT` AS 'NNDT' FROM vnnote a LEFT JOIN hospemp b ON a.NS = b.EMPID WHERE a.`VN` = '$_GET[VN]' AND a.`STATUS` = '1' AND a.`NS` = '$_GET[PROVIDER]'";
								$result = mysql_query($sql);
								$numRows = mysql_num_rows($result);
								
								while($row = mysql_fetch_array($result) ){
									$VN = $row["VN"];
									$NSNOTE = $row["NSNOTE"];
									$PROVIDER = $row["PROVIDER"];
									$PROVIDERNAME = $row["PROVIDERNAME"];
									$NNDT = $row["NNDT"];
								}
								
								if($numRows > 0){
									echo $NSNOTE;
								}
							?>
						</textarea>
						<input type='submit' name='saveNurseNote' id='saveNurseNote' class='btn btn-warning' value='save'>
					</form>
					<?php
						if(isset($_POST['saveNurseNote'])){		
							$queryVnnote = "SELECT NTID FROM vnnote WHERE VN = '$_GET[VN]';";
							$result3 = mysql_query($queryVnnote);
							$numRows3 = mysql_num_rows($result3);
							
							if($numRows3 == 0){
								$sqlInsert = "INSERT INTO vnnote (VN, NSNOTE, NNDT, NS, SYMPTOM, STATUS) VALUES ('$_GET[VN]', '$_POST[nursenote]', NOW(), '$_GET[PROVIDER]', null, '1');";
								$resultInsert = mysql_query($sqlInsert);
								echo "<script language=\"javascript\">window.location='".basename("nurseNote.php?VN=".$_GET['VN']."&PROVIDER=".$_GET['PROVIDER'])."'</script>";
							}elseif($numRows3 > 0){
								$sqlUpdate = "UPDATE vnnote SET STATUS = '0' WHERE VN = '$_GET[VN]' AND NS = '$_GET[PROVIDER]';";
								$resultUpdate = mysql_query($sqlUpdate);
								
								$sqlInsert = "INSERT INTO vnnote (VN, NSNOTE, NNDT, NS, SYMPTOM, STATUS) VALUES ('$_GET[VN]', '$_POST[nursenote]', NOW(), '$_GET[PROVIDER]', null, '1');";
								$resultInsert = mysql_query($sqlInsert);
								echo "<script language=\"javascript\">window.location='".basename("nurseNote.php?VN=".$_GET['VN']."&PROVIDER=".$_GET['PROVIDER'])."'</script>";
							}
						}
					?>
				</div>
			</div>
		</div>

	<?php
		/*
		echo "<h1>test</h1>";
		$fi = new FilesystemIterator(fonts, FilesystemIterator::SKIP_DOTS);
		echo iterator_count($fi);		//count file in folder
		*/
		
		/*
		$dir = "http://172.30.10.2/ryhsystem/FRI/5700000138/img/caseDrawing/5710020001/";
		$file = scandir($dir);
		
		print_r($file);
		*/
		
		/*
		$pathOfPic = "../ryhsystem/FRI/5700000138/img/caseDrawing/5710020001/"
		
		if ($handle = opendir($pathOfPic)) {
			//echo "Directory handle: $handle\n"."<br>";
			//echo "Entries:\n";

			
			while (false !== ($entry = readdir($handle))) {
				echo "$entry"."|";
			}

			closedir($handle);
		}
		*/
	?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script src="ckeditor/ckeditor.js"></script>
	<script>
		// Replace the <textarea id="editor1"> with a CKEditor
		// instance, using default configuration.
		CKEDITOR.replace( 'nursenote', {
			height: '355',
			uiColor: '#FF8C00'
		});
			
		CKEDITOR.config.toolbar = [
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'BGColor', 'Image', 'Print' ] }
			//{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
			//{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
			//{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
			//{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
			//'/',
			//{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
			//{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
			//{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
			//{ name: 'insert', items: [ 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
			//{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
			//'/',
			//{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
			//{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			//{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
			//{ name: 'others', items: [ '-' ] },
			//{ name: 'about', items: [ 'About' ] }
		];
	</script>
	</body>
</html>