<!DOCTYPE html>
<html>
	<?php
		session_start();
		include "include/cdb.php";
		include "class/personClass.php";
		include "class/picManageClass.php";
		include "class/showPicClass.php";
		
		if($_SESSION['USER_ID'] != "" and $_SESSION['USER_NAME'] != ""){
	?>
	<head>
		<meta charset="UTF-8">
		<title>OPD Patient</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- Bootstrap 3.3.2 -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- Font Awesome Icons -->
		<link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- Ionicons -->
		<link href="plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />
		<!-- DATA TABLES -->
		<link href="plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
		<!-- Theme style -->
		<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
		<!-- AdminLTE Skins. Choose a skin from the css/skins 
			 folder instead of downloading all of them to reduce the load. -->
		<link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="skin-blue">
		<!-- Site wrapper -->
		<div class="wrapper">
			<?php
				include "bar/topbar.php";
				include "bar/sidebar.php";

				$objPatInfo = new personClass();
				$objPatInfo->_setHN($_GET['HN']);
				$objPatInfo->getPatientInfo();
			?>

			<!-- =============================================== -->

			<!-- Right side column. Contains the navbar and content of the page -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						OPD Patient
						<small>ข้อมูลผู้ป่วย OPD</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
						<li class='active'><a href="opdPat.php"><i class="fa fa-cart-plus"></i> ข้อมูลผู้ป่วย OPD</a></li>
					</ol>
				</section>

				<!-- Main content -->
				<section class="content">
					<div class='row'>
						<div class='col-md-6'>
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">ข้อมูลผู้ป่วย OPD</h3>&nbsp;
									<!--<a href='articleAdd.php' class='btn btn-default'><i class='fa fa-plus'></i> เพิ่มบทความ</a>-->
								</div><!-- /.box-header -->
								<div class="box-body">
									<form class="form-horizontal" method='post' action=''>
										<div class="form-group">
											<label for="HN" class="col-sm-1 control-label">HN</label>
											<div class="col-sm-5">
												<input class='form-control' type="text" id="HN" name='HN' readonly value='<?php echo $_GET['HN'];?>'>
											</div>

											<label for="VN" class="col-sm-1 control-label">VN</label>
											<div class="col-sm-5">
												<input class='form-control' type="text" id="VN" name='VN' readonly value='<?php echo $_GET['VN'];?>'>
											</div>
										</div>

										<div class="form-group">
											<label for="name" class="col-sm-3 control-label">ชื่อผู้ป่วย</label>
											<div class="col-sm-9">
												<input class='form-control' type="text" id="name" name='name' readonly value='<?php echo $objPatInfo->_getPatName(); ?>'>
											</div>
										</div>
									</form>
								</div><!-- /.box-body -->
							</div><!-- /.box -->
						</div>
						<div class='col-md-6'>
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">รายการผู้ป่วย OPD</h3>&nbsp;
									<!--<a href='articleAdd.php' class='btn btn-default'><i class='fa fa-plus'></i> เพิ่มบทความ</a>-->
								</div><!-- /.box-header -->
								<div class="box-body">
									<form class="form-horizontal" method='post' action='' enctype='multipart/form-data'>
										<div class="form-group">
											<label for="fileUpload" class="col-sm-2 control-label">Upload รูปภาพ</label>
											<div class="col-sm-10">
												<input type="file" id="fileUpload" name='fileUpload'>
												<input type='hidden' name='hn' value='<?php echo $_GET['HN']; ?>'>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10">
												<button type="submit" class="btn btn-default" name='btnUploadPic'>upload</button>

												<?php
													if(isset($_POST['btnUploadPic'])){
														if(!empty($_FILES["fileUpload"]["name"])){
															$objUploadPic = new picManageClass();

															$picName = $objUploadPic->uploadPic($_FILES["fileUpload"]["tmp_name"], $_FILES["fileUpload"]["name"], $_POST['hn']);
															
															//echo $picName;
															echo "<script language=\"javascript\">window.location='".basename("opdPatDetail.php?HN=".$_GET['HN']."&VN=".$_GET['VN'])."'</script>";
														}else{
															echo "<script>alert('"."โปรดกรุณาทำการเลือกรูปภาพ หรือถ่ายภาพก่อนกดปุ่ม Upload"."')</script>";
														}
													}
												?>
											</div>
										</div>

										
									</form>
								</div><!-- /.box-body -->
							</div><!-- /.box -->

							<!--
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">ภาพผู้ป่วย</h3>&nbsp;
								</div>
								<div class="box-body">
									<?php
										/*
										$objShowPicInFolder = new showPicClass();
										$objShowPicInFolder->showPicOfHN($_GET['HN']);
										*/
									?>
								</div>
							</div>
							-->
						</div>
					</div>
				</section><!-- /.content -->
			</div><!-- /.content-wrapper -->
		</div><!-- ./wrapper -->

		<!-- jQuery 2.1.3 -->
		<script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
		<!-- Bootstrap 3.3.2 JS -->
		<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<!-- SlimScroll -->
		<script src="plugins/slimScroll/jquery.slimScroll.min.js" type="text/javascript"></script>
		<!-- FastClick -->
		<script src='plugins/fastclick/fastclick.min.js'></script>
		<!-- AdminLTE App -->
		<script src="dist/js/app.min.js" type="text/javascript"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="dist/js/demo.js" type="text/javascript"></script>
		
		<script>
			$(document).ready(function(){
				$("body").css("display", "none");
				$("body").fadeIn(300);
			});
		</script>

        <!-- DATA TABES SCRIPT -->
		<script src="plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
		<script src="plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
		<script>
			$(function () {
				$("#example1").dataTable();
				//$("#selectedProduct").dataTable();
				$('#example2').dataTable({
					"bPaginate": true,
					"bLengthChange": false,
					"bFilter": false,
					"bSort": true,
					"bInfo": true,
					"bAutoWidth": false
				});
		  	});
		</script>
	</body>
	<?php
		}else{
			echo "<script language='javascript'>";
            echo "alert('โปรดกรุณาเข้าสู่ระบบก่อนการใช้งาน')";
            echo  "</script>";
            echo "<script language=\"javascript\">window.location='".basename("index.php")."'</script>";
		}
	?>
</html>