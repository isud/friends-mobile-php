<!DOCTYPE html>
<html>
	<?php
		session_start();
		include "include/cdb.php";
		include "class/patientListClass.php";
		
		if($_SESSION['USER_ID'] != "" and $_SESSION['USER_NAME'] != ""){
	?>
	<head>
		<meta charset="UTF-8">
		<title>IPD Patient</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- Bootstrap 3.3.2 -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- Font Awesome Icons -->
		<link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- Ionicons -->
		<link href="plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />
		<!-- DATA TABLES -->
		<link href="plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
		<!-- Theme style -->
		<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
		<!-- AdminLTE Skins. Choose a skin from the css/skins 
			 folder instead of downloading all of them to reduce the load. -->
		<link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="skin-blue">
		<!-- Site wrapper -->
		<div class="wrapper">
			<?php
				include "bar/topbar.php";
				include "bar/sidebar.php";

				$objQueryArticle = new patientListClass();
			?>

			<!-- =============================================== -->

			<!-- Right side column. Contains the navbar and content of the page -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						OPD Patient
						<small>ข้อมูลผู้ป่วย IPD</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
						<li class='active'><a href="opdPat.php"><i class="fa fa-cart-plus"></i> ข้อมูลผู้ป่วย IPD</a></li>
					</ol>
				</section>

				<!-- Main content -->
				<section class="content">
					<div class='row'>
						<div class='col-md-12'>
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">รายการผู้ป่วย IPD</h3>&nbsp;
								</div><!-- /.box-header -->
								<div class="box-body">
									<!--
									<div class='row'>
										<div class='col-sm-3'>
											<form method='post' action=''>
												<div class="input-group">
													<?php
														//$objQueryArticle->cmbExaminationRoom();
													?>
													<span class="input-group-btn">
														<button class="btn btn-default" type="submit" name='btnChooseExam'>เลือกแผนก</button>
													</span>
												</div>
												<?php
													/*
													if(isset($_POST['btnChooseExam'])){
														if($_POST['cmbClinicExam'] == ""){
															echo "<script>alert('".'โปรดเลือกห้องตรวจผู้ป่วยก่อน'."')</script>";
														}else{
															$objQueryArticle->patientOPDList($_POST['cmbClinicExam']);
														}
													}
													*/
												?>
											</form>
										</div>
									</div>
									-->
									<div class='row'>
										<div class='col-sm-12'>
											<?php
												$objQueryArticle->patientIPDList();
											?>
										</div>
									</div>
								</div><!-- /.box-body -->
							</div><!-- /.box -->
						</div>
					</div>
				</section><!-- /.content -->
			</div><!-- /.content-wrapper -->
		</div><!-- ./wrapper -->

		<!-- jQuery 2.1.3 -->
		<script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
		<!-- Bootstrap 3.3.2 JS -->
		<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<!-- SlimScroll -->
		<script src="plugins/slimScroll/jquery.slimScroll.min.js" type="text/javascript"></script>
		<!-- FastClick -->
		<script src='plugins/fastclick/fastclick.min.js'></script>
		<!-- AdminLTE App -->
		<script src="dist/js/app.min.js" type="text/javascript"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="dist/js/demo.js" type="text/javascript"></script>
		
		<script>
			$(document).ready(function(){
				$("body").css("display", "none");
				$("body").fadeIn(300);
			});
		</script>

        <!-- DATA TABES SCRIPT -->
		<script src="plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
		<script src="plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
		<script>
			$(function () {
				$("#example1").dataTable();
				//$("#selectedProduct").dataTable();
				$('#example2').dataTable({
					"bPaginate": true,
					"bLengthChange": false,
					"bFilter": false,
					"bSort": true,
					"bInfo": true,
					"bAutoWidth": false
				});
		  	});
		</script>
	</body>
	<?php
		}else{
			echo "<script language='javascript'>";
            echo "alert('โปรดกรุณาเข้าสู่ระบบก่อนการใช้งาน')";
            echo  "</script>";
            echo "<script language=\"javascript\">window.location='".basename("index.php")."'</script>";
		}
	?>
</html>